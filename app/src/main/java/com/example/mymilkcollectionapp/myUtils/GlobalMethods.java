package com.example.mymilkcollectionapp.myUtils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mymilkcollectionapp.R;

public class GlobalMethods {

    public static void showDialog(Context context,MilkTypeInterface milkTypeInterface){
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.milk_type_dialog_inflater);
        Button buffalo = (Button) dialog.findViewById(R.id.btnBuffalo);
        Button cow = (Button) dialog.findViewById(R.id.btnCow);
        Button cancle = (Button) dialog.findViewById(R.id.btnCancle);

        buffalo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                milkTypeInterface.onBuffaloMilkTypeInterface();
            }
        });
        cow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                milkTypeInterface.onCowMilkTypeInterface();
            }
        });
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                milkTypeInterface.onCancleInterface();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public static void editUSerProfileDialog(Context context,EditUserProfileDialogInterface editUserProfileDialogInterface,String title,String value){
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.user_profile_dialog_inflater);
        TextView tvtitle = (TextView) dialog.findViewById(R.id.tvTitle);
        EditText enterValue = (EditText) dialog.findViewById(R.id.etEnterValue);
        Button update = (Button) dialog.findViewById(R.id.btnUpdate);
        Button cancle = (Button) dialog.findViewById(R.id.btnCancle);
        tvtitle.setText(title);
        enterValue.setText(value);


//        update.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                editUserProfileDialogInterface.onUpdateInterface(enterValue.getText().toString());
//            }
//        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                editUserProfileDialogInterface.onUpdateInterface(enterValue.getText().toString());
            }
        });
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editUserProfileDialogInterface.onCancleInterface();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
