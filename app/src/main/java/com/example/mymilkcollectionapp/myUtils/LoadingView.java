package com.example.mymilkcollectionapp.myUtils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import com.example.mymilkcollectionapp.R;

public class LoadingView {

    Dialog dialog;
    Context context;

    public LoadingView(Context context) {
        this.context = context;
        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_view);
    }

    public void showDialog(){
        if(!((Activity)context).isFinishing()){
            try {
                dialog.show();
            }catch (WindowManager.BadTokenException e){

            }
        }
    }

    public void hideDialog(){
        dialog.dismiss();
    }
}
