package com.example.mymilkcollectionapp.myUtils;

public interface MilkTypeInterface {

    void onBuffaloMilkTypeInterface();
    void onCowMilkTypeInterface();
    void onCancleInterface();
}
