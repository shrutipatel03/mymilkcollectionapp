package com.example.mymilkcollectionapp.myUtils;

public interface EditUserProfileDialogInterface {

    void onUpdateInterface(String enterValue);
    void onCancleInterface();
}
