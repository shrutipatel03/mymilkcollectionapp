package com.example.mymilkcollectionapp.myUtils;

import com.example.mymilkcollectionapp.model.CustomerWiseReportModel;
import com.example.mymilkcollectionapp.model.CustomerWiseReportResponseModel;
import com.example.mymilkcollectionapp.model.FatWisePriceResponseModel;
import com.example.mymilkcollectionapp.model.GetUserWisePriceResponceModel;
import com.example.mymilkcollectionapp.model.PaymentReportResponseModel;
import com.example.mymilkcollectionapp.model.SNFFatAllListResponceModel;
import com.example.mymilkcollectionapp.model.GetCustomerListResponseModel;
import com.example.mymilkcollectionapp.model.GetFixPriceResponseModel;
import com.example.mymilkcollectionapp.model.GetUserWiseMilkEntryResponseModel;
import com.example.mymilkcollectionapp.model.LoginResponseModel;
import com.example.mymilkcollectionapp.model.NewCustomerResponseModel;
import com.example.mymilkcollectionapp.model.ShiftWiseGeneralReportModel;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.model.UserRegisterModel;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("UserController/user_register")
    Call<UserRegisterModel> registerUser(@Field("dairy_name") String dairyName,
                                         @Field("dairy_owner_name") String dairyOwnerName,
                                         @Field("mobile_number") String mobileNumber,
                                         @Field("email") String email,
                                         @Field("password") String password,
                                         @Field("dairy_id") int dairy_id,
                                         @Field("village") String village,
                                         @Field("taluka") String taluka,
                                         @Field("district") String district,
                                         @Field("pincode") String pincode);

    @FormUrlEncoded
    @POST("UserController/login_user")
    Call<LoginResponseModel> getLoginData(@Field("user_mobile") String user_mobile, @Field("user_password") String user_password);

    @FormUrlEncoded
    @POST("CustomersController/add_customer")
    Call<NewCustomerResponseModel> getCustomerData(@Field("customer_code") String customerCode,
                                                   @Field("customer_name") String customerName,
                                                   @Field("customer_mobile_number") String customerMobileNumber,
                                                   @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("CustomersController/getCustomerList")
    Call<GetCustomerListResponseModel> getCustomerList(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST("CustomersController/customer_update")
    Call<StatusModel> updateCustomerData(@Field("customer_code") String customerCode,
                                         @Field("customer_name") String customerName,
                                         @Field("customer_mobile_number") String customerMobileNumber,
                                         @Field("user_id") String userId,
                                         @Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("CustomersController/delete_customer")
    Call<StatusModel> deleteCustomerData(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("MilkController/add_milk")
    Call<StatusModel> addMilkData(@Field("user_id") String user_id,
                                  @Field("customer_id") String customer_id,
                                  @Field("customer_code") String customer_code,
                                  @Field("customer_name") String customer_name,
                                  @Field("milk_type") String milk_type,
                                  @Field("selected_date") String selected_date,
                                  @Field("shift") String shift,
                                  @Field("weight") String weight,
                                  @Field("fat") String fat,
                                  @Field("fat_price") String fat_price,
                                  @Field("weight_type") String weight_type,
                                  @Field("snf") String snf,
                                  @Field("clr") String clr,
                                  @Field("total_price") String total_price,
                                  @Field("per_unit_price") String per_unit_price,
                                  @Field("price_calculated_by") String price_calculated_by);

    @FormUrlEncoded
    @POST("MilkController/get_user_wise_milk_entry")
    Call<GetUserWiseMilkEntryResponseModel> getUserWiseMilkEntry(@Field("user_id") String user_id,
                                                                 @Field("shift") String shift,
                                                                 @Field("date") String date);

    @FormUrlEncoded
    @POST("MilkController/update_milk")
    Call<StatusModel> updateMilk(@Field("user_id") String user_id,
                                 @Field("customer_id") String customer_id,
                                 @Field("customer_code") String customer_code,
                                 @Field("customer_name") String customer_name,
                                 @Field("milk_type") String milk_type,
                                 @Field("selected_date") String selected_date,
                                 @Field("shift") String shift,
                                 @Field("weight") String weight,
                                 @Field("fat") String fat,
                                 @Field("fat_price") String fat_price,
                                 @Field("weight_type") String weight_type,
                                 @Field("snf") String snf,
                                 @Field("clr") String clr,
                                 @Field("total_price") String total_price,
                                 @Field("per_unit_price") String per_unit_price,
                                 @Field("price_calculated_by") String price_calculated_by,
                                 @Field("me_id") String me_id);

    @FormUrlEncoded
    @POST("MilkController/delete_milk_entry")
    Call<StatusModel> deleteMilkEntry(@Field("me_id") String me_id);

    @FormUrlEncoded
    @POST("PriceController/get_fat_wise_price")
    Call<FatWisePriceResponseModel> getFatWisePrice(@Field("fp_u_id") String fp_u_id,
                                                    @Field("fs_type") String fs_type,
                                                    @Field("fs_milk_type") String fs_milk_type,
                                                    @Field("fs_parent_id") String fs_parent_id);

    @POST("PriceController/update_fat_price")
    Call<StatusModel> updateAdminFatRate(@Body JsonObject body);

    @FormUrlEncoded
    @POST("PriceController/get_fat_wise_price_all")
    Call<GetFixPriceResponseModel> getFixPrice(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("PriceController/update_fat_wise_price")
    Call<StatusModel> updateFixPrice(@Field("user_id") String user_id,
                                         @Field("milk_type") String milk_type,
                                         @Field("total_price") String total_price,
                                         @Field("fat_wise_price") String fat_wise_price);

    @POST("FatSnfController/get_snf_fat_all_list")
    Call<SNFFatAllListResponceModel> getAllSnfAndFat();

    @FormUrlEncoded
    @POST("PriceController/get_user_wise_price")
    Call<GetUserWisePriceResponceModel> getUserWisePrice(@Field("fp_u_id") String fp_u_id);

    @FormUrlEncoded
    @POST("ReportController/shift_wise_general_report")
    Call<ShiftWiseGeneralReportModel> getShiftWiseGenReport(@Field("user_id") String user_id,
                                                            @Field("shift") String shift,
                                                            @Field("date") String date);
    @FormUrlEncoded
    @POST("ReportController/get_customer_payment_report")
    Call<PaymentReportResponseModel> getPaymentReport(@Field("start_date") String start_date,
                                                      @Field("end_date") String end_date,
                                                      @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("ReportController/get_customer_wise_report")
    Call<CustomerWiseReportResponseModel> getCustomerWiseReport(@Field("start_date") String start_date,
                                                                @Field("end_date") String end_date,
                                                                @Field("user_id") String user_id,
                                                                @Field("cust_code") String cust_code);

    @FormUrlEncoded
    @POST("UserController/change_user_password")
    Call<StatusModel> changeUserPassword(@Field("user_password") String userPassword,
                                         @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("UserController/update_user_detail")
    Call<StatusModel> updateUserDetail(@Field("user_id") String user_id,
                                       @Field("change_type") String change_type,
                                       @Field("value") String value);
}
