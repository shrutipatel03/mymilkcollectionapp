package com.example.mymilkcollectionapp.myUtils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

public class PrintDataManager {
    String TAG = "PrintDataManager";
    Context context;
    public static PrintDataManager printDataManager;
    public static BluetoothAdapter mBluetoothAdapter = null;
    public boolean isBluetoothSupport;
    Activity activity;
    static private final int REQUEST_ENABLE_BT = 0*1000;
    static private BluetoothSocket mbtSocket = null;
    public PrintDataManager(Context context,Activity activity)
    {
        this.activity=activity;
        this.context=context;
        mBluetoothAdapter= BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter==null)
        {
            isBluetoothSupport=false;
        }
        else
        {
            isBluetoothSupport=true;
        }
    }
    public  BluetoothSocket getSocket() {
        return mbtSocket;
    }
    public void flushData() {
        try {
            if (mbtSocket != null) {
                mbtSocket.close();
                mbtSocket = null;
            }

            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.cancelDiscovery();
            }





            //finalize();
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
        }

    }

    public void startBluetoothRequest()
    {
        Intent enableBtIntent = new Intent(
                BluetoothAdapter.ACTION_REQUEST_ENABLE);
        try {
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } catch (Exception ex) {

            Log.d(TAG, "startBluetoothRequest: "+ex.getMessage());
            //return -2;
        }
    }

    public static boolean setBluetooth(boolean enable) {
        //BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = mBluetoothAdapter.isEnabled();
        if (enable && !isEnabled) {
            return mBluetoothAdapter.enable();
        }
        else if(!enable && isEnabled) {
            return mBluetoothAdapter.disable();
        }
        // No need to change bluetooth state
        return true;
    }
    public Set<BluetoothDevice> getConnectedDevices()
    {
        if(mBluetoothAdapter!=null)
        {
            return mBluetoothAdapter.getBondedDevices();
        }
        else
        {
            return null;
        }
    }

    public void createSocket(BluetoothDevice bluetoothDevice)
    {

        try {
            Method m = bluetoothDevice.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
            mbtSocket = (BluetoothSocket) m.invoke(bluetoothDevice, 1);
            mbtSocket.connect();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
