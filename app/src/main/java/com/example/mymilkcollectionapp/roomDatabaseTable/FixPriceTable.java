package com.example.mymilkcollectionapp.roomDatabaseTable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class FixPriceTable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ColumnInfo(name = "fwp_id")
    private String fwpId;

    @ColumnInfo(name = "fwp_total_kg_fat_price")
    private String fwpTotalKgFatPrice;

    @ColumnInfo(name = "fwp_fat_wise_price")
    private String fwpFatWisePrice;

    @ColumnInfo(name = "fwp_milk_type")
    private String fwpMilkType;

    @ColumnInfo(name = "fwp_user_id")
    private String fwpUserId;

    @ColumnInfo(name = "fwp_inserted_date")
    private String fwpInsertedDate;

    @ColumnInfo(name = "fwp_update_date")
    private String fwpUpdateDate;

    public FixPriceTable() {
    }

    public String getFwpId() {
        return fwpId;
    }

    public void setFwpId(String fwpId) {
        this.fwpId = fwpId;
    }

    public String getFwpTotalKgFatPrice() {
        return fwpTotalKgFatPrice;
    }

    public void setFwpTotalKgFatPrice(String fwpTotalKgFatPrice) {
        this.fwpTotalKgFatPrice = fwpTotalKgFatPrice;
    }

    public String getFwpFatWisePrice() {
        return fwpFatWisePrice;
    }

    public void setFwpFatWisePrice(String fwpFatWisePrice) {
        this.fwpFatWisePrice = fwpFatWisePrice;
    }

    public String getFwpMilkType() {
        return fwpMilkType;
    }

    public void setFwpMilkType(String fwpMilkType) {
        this.fwpMilkType = fwpMilkType;
    }

    public String getFwpUserId() {
        return fwpUserId;
    }

    public void setFwpUserId(String fwpUserId) {
        this.fwpUserId = fwpUserId;
    }

    public String getFwpInsertedDate() {
        return fwpInsertedDate;
    }

    public void setFwpInsertedDate(String fwpInsertedDate) {
        this.fwpInsertedDate = fwpInsertedDate;
    }

    public String getFwpUpdateDate() {
        return fwpUpdateDate;
    }

    public void setFwpUpdateDate(String fwpUpdateDate) {
        this.fwpUpdateDate = fwpUpdateDate;
    }
}
