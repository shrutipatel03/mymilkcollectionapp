package com.example.mymilkcollectionapp.roomDatabaseTable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class FatandSNFPriceTable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "fs_id")
    private String fsId;

    @ColumnInfo(name = "fs_type")
    private String fsType;

    @ColumnInfo(name = "fs_value")
    private String fsValue;

    @ColumnInfo(name = "fs_milk_type")
    private String fsMilkType;

    @ColumnInfo(name = "fs_parent_id")
    private String fsParentId;

    @ColumnInfo(name = "fs_insert_date")
    private String fsInsertDate;

    @ColumnInfo(name = "fs_update_date")
    private String fsUpdateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFsId() {
        return fsId;
    }

    public void setFsId(String fsId) {
        this.fsId = fsId;
    }

    public String getFsType() {
        return fsType;
    }

    public void setFsType(String fsType) {
        this.fsType = fsType;
    }

    public String getFsValue() {
        return fsValue;
    }

    public void setFsValue(String fsValue) {
        this.fsValue = fsValue;
    }

    public String getFsMilkType() {
        return fsMilkType;
    }

    public void setFsMilkType(String fsMilkType) {
        this.fsMilkType = fsMilkType;
    }

    public String getFsParentId() {
        return fsParentId;
    }

    public void setFsParentId(String fsParentId) {
        this.fsParentId = fsParentId;
    }

    public String getFsInsertDate() {
        return fsInsertDate;
    }

    public void setFsInsertDate(String fsInsertDate) {
        this.fsInsertDate = fsInsertDate;
    }

    public String getFsUpdateDate() {
        return fsUpdateDate;
    }

    public void setFsUpdateDate(String fsUpdateDate) {
        this.fsUpdateDate = fsUpdateDate;
    }
}
