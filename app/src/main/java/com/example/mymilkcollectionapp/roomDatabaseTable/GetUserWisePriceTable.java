package com.example.mymilkcollectionapp.roomDatabaseTable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class GetUserWisePriceTable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "fp_id")
    private String fpId;

    @ColumnInfo(name = "fp_u_id")
    private String fpUId;

    @ColumnInfo(name = "fp_fs_id")
    private String fpFsId;

    @ColumnInfo(name = "fp_rate")
    private String fpRate;

    @ColumnInfo(name = "fp_inserted_date")
    private String fpInsertedDate;

    @ColumnInfo(name = "fp_updated_date")
    private String fpUpdatedDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFpId() {
        return fpId;
    }

    public void setFpId(String fpId) {
        this.fpId = fpId;
    }

    public String getFpUId() {
        return fpUId;
    }

    public void setFpUId(String fpUId) {
        this.fpUId = fpUId;
    }

    public String getFpFsId() {
        return fpFsId;
    }

    public void setFpFsId(String fpFsId) {
        this.fpFsId = fpFsId;
    }

    public String getFpRate() {
        return fpRate;
    }

    public void setFpRate(String fpRate) {
        this.fpRate = fpRate;
    }

    public String getFpInsertedDate() {
        return fpInsertedDate;
    }

    public void setFpInsertedDate(String fpInsertedDate) {
        this.fpInsertedDate = fpInsertedDate;
    }

    public String getFpUpdatedDate() {
        return fpUpdatedDate;
    }

    public void setFpUpdatedDate(String fpUpdatedDate) {
        this.fpUpdatedDate = fpUpdatedDate;
    }
}
