package com.example.mymilkcollectionapp.model;

import androidx.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GetFixPriceModel {
    @SerializedName("fwp_id")
    @Expose
    private String fwpId;
    @SerializedName("fwp_total_kg_fat_price")
    @Expose
    private String fwpTotalKgFatPrice;
    @SerializedName("fwp_fat_wise_price")
    @Expose
    private String fwpFatWisePrice;
    @SerializedName("fwp_milk_type")
    @Expose
    private String fwpMilkType;
    @SerializedName("fwp_user_id")
    @Expose
    private String fwpUserId;
    @SerializedName("fwp_inserted_date")
    @Expose
    private String fwpInsertedDate;
    @SerializedName("fwp_update_date")
    @Expose
    private String fwpUpdateDate;

    public String getFwpId() {
        return fwpId;
    }

    public void setFwpId(String fwpId) {
        this.fwpId = fwpId;
    }

    public String getFwpTotalKgFatPrice() {
        return fwpTotalKgFatPrice;
    }

    public void setFwpTotalKgFatPrice(String fwpTotalKgFatPrice) {
        this.fwpTotalKgFatPrice = fwpTotalKgFatPrice;
    }

    public String getFwpFatWisePrice() {
        return fwpFatWisePrice;
    }

    public void setFwpFatWisePrice(String fwpFatWisePrice) {
        this.fwpFatWisePrice = fwpFatWisePrice;
    }

    public String getFwpMilkType() {
        return fwpMilkType;
    }

    public void setFwpMilkType(String fwpMilkType) {
        this.fwpMilkType = fwpMilkType;
    }

    public String getFwpUserId() {
        return fwpUserId;
    }

    public void setFwpUserId(String fwpUserId) {
        this.fwpUserId = fwpUserId;
    }

    public String getFwpInsertedDate() {
        return fwpInsertedDate;
    }

    public void setFwpInsertedDate(String fwpInsertedDate) {
        this.fwpInsertedDate = fwpInsertedDate;
    }

    public String getFwpUpdateDate() {
        return fwpUpdateDate;
    }

    public void setFwpUpdateDate(String fwpUpdateDate) {
        this.fwpUpdateDate = fwpUpdateDate;
    }

}
