package com.example.mymilkcollectionapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SNFFatAllListModel {
    @SerializedName("fs_id")
    @Expose
    private String fsId;
    @SerializedName("fs_type")
    @Expose
    private String fsType;
    @SerializedName("fs_value")
    @Expose
    private String fsValue;
    @SerializedName("fs_milk_type")
    @Expose
    private String fsMilkType;
    @SerializedName("fs_parent_id")
    @Expose
    private String fsParentId;
    @SerializedName("fs_insert_date")
    @Expose
    private String fsInsertDate;
    @SerializedName("fs_update_date")
    @Expose
    private String fsUpdateDate;

    public String getFsId() {
        return fsId;
    }

    public void setFsId(String fsId) {
        this.fsId = fsId;
    }

    public String getFsType() {
        return fsType;
    }

    public void setFsType(String fsType) {
        this.fsType = fsType;
    }

    public String getFsValue() {
        return fsValue;
    }

    public void setFsValue(String fsValue) {
        this.fsValue = fsValue;
    }

    public String getFsMilkType() {
        return fsMilkType;
    }

    public void setFsMilkType(String fsMilkType) {
        this.fsMilkType = fsMilkType;
    }

    public String getFsParentId() {
        return fsParentId;
    }

    public void setFsParentId(String fsParentId) {
        this.fsParentId = fsParentId;
    }

    public String getFsInsertDate() {
        return fsInsertDate;
    }

    public void setFsInsertDate(String fsInsertDate) {
        this.fsInsertDate = fsInsertDate;
    }

    public String getFsUpdateDate() {
        return fsUpdateDate;
    }

    public void setFsUpdateDate(String fsUpdateDate) {
        this.fsUpdateDate = fsUpdateDate;
    }
}
