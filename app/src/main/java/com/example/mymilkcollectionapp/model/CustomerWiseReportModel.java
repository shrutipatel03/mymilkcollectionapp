package com.example.mymilkcollectionapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerWiseReportModel {
    @SerializedName("me_id")
    @Expose
    private String meId;
    @SerializedName("me_local_id")
    @Expose
    private String meLocalId;
    @SerializedName("me_user_id")
    @Expose
    private String meUserId;
    @SerializedName("me_customer_id")
    @Expose
    private String meCustomerId;
    @SerializedName("me_customer_code")
    @Expose
    private String meCustomerCode;
    @SerializedName("me_customer_name")
    @Expose
    private String meCustomerName;
    @SerializedName("me_milk_type")
    @Expose
    private String meMilkType;
    @SerializedName("me_date")
    @Expose
    private String meDate;
    @SerializedName("me_shift")
    @Expose
    private String meShift;
    @SerializedName("me_weight")
    @Expose
    private String meWeight;
    @SerializedName("me_fat")
    @Expose
    private String meFat;
    @SerializedName("me_fat_price")
    @Expose
    private String meFatPrice;
    @SerializedName("me_weight_type")
    @Expose
    private String meWeightType;
    @SerializedName("me_snf")
    @Expose
    private String meSnf;
    @SerializedName("me_clr")
    @Expose
    private String meClr;
    @SerializedName("me_total_price")
    @Expose
    private String meTotalPrice;
    @SerializedName("me_per_unit_price")
    @Expose
    private String mePerUnitPrice;
    @SerializedName("me_price_calculated_by")
    @Expose
    private String mePriceCalculatedBy;
    @SerializedName("me_inserted_date")
    @Expose
    private String meInsertedDate;
    @SerializedName("me_update_date")
    @Expose
    private String meUpdateDate;

    public String getMeId() {
        return meId;
    }

    public void setMeId(String meId) {
        this.meId = meId;
    }

    public String getMeLocalId() {
        return meLocalId;
    }

    public void setMeLocalId(String meLocalId) {
        this.meLocalId = meLocalId;
    }

    public String getMeUserId() {
        return meUserId;
    }

    public void setMeUserId(String meUserId) {
        this.meUserId = meUserId;
    }

    public String getMeCustomerId() {
        return meCustomerId;
    }

    public void setMeCustomerId(String meCustomerId) {
        this.meCustomerId = meCustomerId;
    }

    public String getMeCustomerCode() {
        return meCustomerCode;
    }

    public void setMeCustomerCode(String meCustomerCode) {
        this.meCustomerCode = meCustomerCode;
    }

    public String getMeCustomerName() {
        return meCustomerName;
    }

    public void setMeCustomerName(String meCustomerName) {
        this.meCustomerName = meCustomerName;
    }

    public String getMeMilkType() {
        return meMilkType;
    }

    public void setMeMilkType(String meMilkType) {
        this.meMilkType = meMilkType;
    }

    public String getMeDate() {
        return meDate;
    }

    public void setMeDate(String meDate) {
        this.meDate = meDate;
    }

    public String getMeShift() {
        return meShift;
    }

    public void setMeShift(String meShift) {
        this.meShift = meShift;
    }

    public String getMeWeight() {
        return meWeight;
    }

    public void setMeWeight(String meWeight) {
        this.meWeight = meWeight;
    }

    public String getMeFat() {
        return meFat;
    }

    public void setMeFat(String meFat) {
        this.meFat = meFat;
    }

    public String getMeFatPrice() {
        return meFatPrice;
    }

    public void setMeFatPrice(String meFatPrice) {
        this.meFatPrice = meFatPrice;
    }

    public String getMeWeightType() {
        return meWeightType;
    }

    public void setMeWeightType(String meWeightType) {
        this.meWeightType = meWeightType;
    }

    public String getMeSnf() {
        return meSnf;
    }

    public void setMeSnf(String meSnf) {
        this.meSnf = meSnf;
    }

    public String getMeClr() {
        return meClr;
    }

    public void setMeClr(String meClr) {
        this.meClr = meClr;
    }

    public String getMeTotalPrice() {
        return meTotalPrice;
    }

    public void setMeTotalPrice(String meTotalPrice) {
        this.meTotalPrice = meTotalPrice;
    }

    public String getMePerUnitPrice() {
        return mePerUnitPrice;
    }

    public void setMePerUnitPrice(String mePerUnitPrice) {
        this.mePerUnitPrice = mePerUnitPrice;
    }

    public String getMePriceCalculatedBy() {
        return mePriceCalculatedBy;
    }

    public void setMePriceCalculatedBy(String mePriceCalculatedBy) {
        this.mePriceCalculatedBy = mePriceCalculatedBy;
    }

    public String getMeInsertedDate() {
        return meInsertedDate;
    }

    public void setMeInsertedDate(String meInsertedDate) {
        this.meInsertedDate = meInsertedDate;
    }

    public String getMeUpdateDate() {
        return meUpdateDate;
    }

    public void setMeUpdateDate(String meUpdateDate) {
        this.meUpdateDate = meUpdateDate;
    }

}
