package com.example.mymilkcollectionapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShiftWiseGeneralReportModel {

    @SerializedName("total_milk")
    @Expose
    private String totalMilk;
    @SerializedName("total_rs")
    @Expose
    private String totalRs;
    @SerializedName("total_avg_fat")
    @Expose
    private String totalAvgFat;
    @SerializedName("buf_milk_ltr")
    @Expose
    private String bufMilkLtr;
    @SerializedName("buf_rs")
    @Expose
    private String bufRs;
    @SerializedName("buf_avg_fat")
    @Expose
    private String bufAvgFat;
    @SerializedName("cow_milk_ltr")
    @Expose
    private String cowMilkLtr;
    @SerializedName("cow_rs")
    @Expose
    private String cowRs;
    @SerializedName("cow_avg_fat")
    @Expose
    private String cowAvgFat;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("shift")
    @Expose
    private String shift;

    public String getTotalMilk() {
        return totalMilk;
    }

    public void setTotalMilk(String totalMilk) {
        this.totalMilk = totalMilk;
    }

    public String getTotalRs() {
        return totalRs;
    }

    public void setTotalRs(String totalRs) {
        this.totalRs = totalRs;
    }

    public String getTotalAvgFat() {
        return totalAvgFat;
    }

    public void setTotalAvgFat(String totalAvgFat) {
        this.totalAvgFat = totalAvgFat;
    }

    public String getBufMilkLtr() {
        return bufMilkLtr;
    }

    public void setBufMilkLtr(String bufMilkLtr) {
        this.bufMilkLtr = bufMilkLtr;
    }

    public String getBufRs() {
        return bufRs;
    }

    public void setBufRs(String bufRs) {
        this.bufRs = bufRs;
    }

    public String getBufAvgFat() {
        return bufAvgFat;
    }

    public void setBufAvgFat(String bufAvgFat) {
        this.bufAvgFat = bufAvgFat;
    }

    public String getCowMilkLtr() {
        return cowMilkLtr;
    }

    public void setCowMilkLtr(String cowMilkLtr) {
        this.cowMilkLtr = cowMilkLtr;
    }

    public String getCowRs() {
        return cowRs;
    }

    public void setCowRs(String cowRs) {
        this.cowRs = cowRs;
    }

    public String getCowAvgFat() {
        return cowAvgFat;
    }

    public void setCowAvgFat(String cowAvgFat) {
        this.cowAvgFat = cowAvgFat;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }
}
