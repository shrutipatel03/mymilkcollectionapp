package com.example.mymilkcollectionapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FatWisePriceModel implements Parcelable {
    @SerializedName("fs_id")
    @Expose
    private String fsId;
    @SerializedName("fs_type")
    @Expose
    private String fsType;
    @SerializedName("fs_value")
    @Expose
    private String fsValue;
    @SerializedName("fs_milk_type")
    @Expose
    private String fsMilkType;
    @SerializedName("fs_parent_id")
    @Expose
    private String fsParentId;
    @SerializedName("fs_insert_date")
    @Expose
    private String fsInsertDate;
    @SerializedName("fs_update_date")
    @Expose
    private String fsUpdateDate;
    @SerializedName("fp_id")
    @Expose
    private String fpId;
    @SerializedName("fp_u_id")
    @Expose
    private String fpUId;
    @SerializedName("fp_fs_id")
    @Expose
    private String fpFsId;
    @SerializedName("fp_rate")
    @Expose
    private String fpRate;
    @SerializedName("fp_inserted_date")
    @Expose
    private String fpInsertedDate;
    @SerializedName("fp_updated_date")
    @Expose
    private String fpUpdatedDate;

    protected FatWisePriceModel(Parcel in) {
        fsId = in.readString();
        fsType = in.readString();
        fsValue = in.readString();
        fsMilkType = in.readString();
        fsParentId = in.readString();
        fsInsertDate = in.readString();
        fsUpdateDate = in.readString();
        fpId = in.readString();
        fpUId = in.readString();
        fpFsId = in.readString();
        fpRate = in.readString();
        fpInsertedDate = in.readString();
        fpUpdatedDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fsId);
        dest.writeString(fsType);
        dest.writeString(fsValue);
        dest.writeString(fsMilkType);
        dest.writeString(fsParentId);
        dest.writeString(fsInsertDate);
        dest.writeString(fsUpdateDate);
        dest.writeString(fpId);
        dest.writeString(fpUId);
        dest.writeString(fpFsId);
        dest.writeString(fpRate);
        dest.writeString(fpInsertedDate);
        dest.writeString(fpUpdatedDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FatWisePriceModel> CREATOR = new Creator<FatWisePriceModel>() {
        @Override
        public FatWisePriceModel createFromParcel(Parcel in) {
            return new FatWisePriceModel(in);
        }

        @Override
        public FatWisePriceModel[] newArray(int size) {
            return new FatWisePriceModel[size];
        }
    };

    public String getFsId() {
        return fsId;
    }

    public void setFsId(String fsId) {
        this.fsId = fsId;
    }

    public String getFsType() {
        return fsType;
    }

    public void setFsType(String fsType) {
        this.fsType = fsType;
    }

    public String getFsValue() {
        return fsValue;
    }

    public void setFsValue(String fsValue) {
        this.fsValue = fsValue;
    }

    public String getFsMilkType() {
        return fsMilkType;
    }

    public void setFsMilkType(String fsMilkType) {
        this.fsMilkType = fsMilkType;
    }

    public String getFsParentId() {
        return fsParentId;
    }

    public void setFsParentId(String fsParentId) {
        this.fsParentId = fsParentId;
    }

    public String getFsInsertDate() {
        return fsInsertDate;
    }

    public void setFsInsertDate(String fsInsertDate) {
        this.fsInsertDate = fsInsertDate;
    }

    public String getFsUpdateDate() {
        return fsUpdateDate;
    }

    public void setFsUpdateDate(String fsUpdateDate) {
        this.fsUpdateDate = fsUpdateDate;
    }

    public String getFpId() {
        return fpId;
    }

    public void setFpId(String fpId) {
        this.fpId = fpId;
    }

    public String getFpUId() {
        return fpUId;
    }

    public void setFpUId(String fpUId) {
        this.fpUId = fpUId;
    }

    public String getFpFsId() {
        return fpFsId;
    }

    public void setFpFsId(String fpFsId) {
        this.fpFsId = fpFsId;
    }

    public String getFpRate() {
        return fpRate;
    }

    public void setFpRate(String fpRate) {
        this.fpRate = fpRate;
    }

    public String getFpInsertedDate() {
        return fpInsertedDate;
    }

    public void setFpInsertedDate(String fpInsertedDate) {
        this.fpInsertedDate = fpInsertedDate;
    }

    public String getFpUpdatedDate() {
        return fpUpdatedDate;
    }

    public void setFpUpdatedDate(String fpUpdatedDate) {
        this.fpUpdatedDate = fpUpdatedDate;
    }
}
