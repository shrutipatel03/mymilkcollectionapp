package com.example.mymilkcollectionapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserWisePriceModel {
    @SerializedName("fp_id")
    @Expose
    private String fpId;
    @SerializedName("fp_u_id")
    @Expose
    private String fpUId;
    @SerializedName("fp_fs_id")
    @Expose
    private String fpFsId;
    @SerializedName("fp_rate")
    @Expose
    private String fpRate;
    @SerializedName("fp_inserted_date")
    @Expose
    private String fpInsertedDate;
    @SerializedName("fp_updated_date")
    @Expose
    private String fpUpdatedDate;

    public String getFpId() {
        return fpId;
    }

    public void setFpId(String fpId) {
        this.fpId = fpId;
    }

    public String getFpUId() {
        return fpUId;
    }

    public void setFpUId(String fpUId) {
        this.fpUId = fpUId;
    }

    public String getFpFsId() {
        return fpFsId;
    }

    public void setFpFsId(String fpFsId) {
        this.fpFsId = fpFsId;
    }

    public String getFpRate() {
        return fpRate;
    }

    public void setFpRate(String fpRate) {
        this.fpRate = fpRate;
    }

    public String getFpInsertedDate() {
        return fpInsertedDate;
    }

    public void setFpInsertedDate(String fpInsertedDate) {
        this.fpInsertedDate = fpInsertedDate;
    }

    public String getFpUpdatedDate() {
        return fpUpdatedDate;
    }

    public void setFpUpdatedDate(String fpUpdatedDate) {
        this.fpUpdatedDate = fpUpdatedDate;
    }

}
