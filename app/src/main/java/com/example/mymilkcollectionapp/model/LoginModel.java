package com.example.mymilkcollectionapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("user_dairy_name")
    @Expose
    private String userDairyName;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_mobile_number")
    @Expose
    private String userMobileNumber;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("user_is_active")
    @Expose
    private String userIsActive;
    @SerializedName("user_deactive_message")
    @Expose
    private String userDeactiveMessage;
    @SerializedName("dairy_id")
    @Expose
    private String dairyId;
    @SerializedName("village")
    @Expose
    private String village;
    @SerializedName("taluka")
    @Expose
    private String taluka;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("expire_date")
    @Expose
    private String expireDate;
    @SerializedName("user_insert_date")
    @Expose
    private String userInsertDate;
    @SerializedName("user_update_date")
    @Expose
    private String userUpdateDate;
    @SerializedName("dairy_name")
    @Expose
    private String dairyName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserDairyName() {
        return userDairyName;
    }

    public void setUserDairyName(String userDairyName) {
        this.userDairyName = userDairyName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMobileNumber() {
        return userMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        this.userMobileNumber = userMobileNumber;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserIsActive() {
        return userIsActive;
    }

    public void setUserIsActive(String userIsActive) {
        this.userIsActive = userIsActive;
    }

    public String getUserDeactiveMessage() {
        return userDeactiveMessage;
    }

    public void setUserDeactiveMessage(String userDeactiveMessage) {
        this.userDeactiveMessage = userDeactiveMessage;
    }

    public String getDairyId() {
        return dairyId;
    }

    public void setDairyId(String dairyId) {
        this.dairyId = dairyId;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getTaluka() {
        return taluka;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getUserInsertDate() {
        return userInsertDate;
    }

    public void setUserInsertDate(String userInsertDate) {
        this.userInsertDate = userInsertDate;
    }

    public String getUserUpdateDate() {
        return userUpdateDate;
    }

    public void setUserUpdateDate(String userUpdateDate) {
        this.userUpdateDate = userUpdateDate;
    }

    public String getDairyName() {
        return dairyName;
    }

    public void setDairyName(String dairyName) {
        this.dairyName = dairyName;
    }
}
