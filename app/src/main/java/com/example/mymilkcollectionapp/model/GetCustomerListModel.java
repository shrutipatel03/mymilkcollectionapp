package com.example.mymilkcollectionapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCustomerListModel implements Parcelable {
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("customer_local_id")
    @Expose
    private String customerLocalId;
    @SerializedName("customer_code")
    @Expose
    private String customerCode;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_mobile_number")
    @Expose
    private String customerMobileNumber;
    @SerializedName("customer_mobile_number_2")
    @Expose
    private String customerMobileNumber2;
    @SerializedName("customer_bank_name")
    @Expose
    private String customerBankName;
    @SerializedName("customer_bank_account_number")
    @Expose
    private String customerBankAccountNumber;
    @SerializedName("customer_bank_ifccode")
    @Expose
    private String customerBankIfccode;
    @SerializedName("customer_payment_mode")
    @Expose
    private String customerPaymentMode;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("inserted_date")
    @Expose
    private String insertedDate;
    @SerializedName("update_date")
    @Expose
    private String updateDate;

    public GetCustomerListModel() {
    }

    protected GetCustomerListModel(Parcel in) {
        customerId = in.readString();
        customerLocalId = in.readString();
        customerCode = in.readString();
        customerName = in.readString();
        customerMobileNumber = in.readString();
        customerMobileNumber2 = in.readString();
        customerBankName = in.readString();
        customerBankAccountNumber = in.readString();
        customerBankIfccode = in.readString();
        customerPaymentMode = in.readString();
        userId = in.readString();
        insertedDate = in.readString();
        updateDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerId);
        dest.writeString(customerLocalId);
        dest.writeString(customerCode);
        dest.writeString(customerName);
        dest.writeString(customerMobileNumber);
        dest.writeString(customerMobileNumber2);
        dest.writeString(customerBankName);
        dest.writeString(customerBankAccountNumber);
        dest.writeString(customerBankIfccode);
        dest.writeString(customerPaymentMode);
        dest.writeString(userId);
        dest.writeString(insertedDate);
        dest.writeString(updateDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetCustomerListModel> CREATOR = new Creator<GetCustomerListModel>() {
        @Override
        public GetCustomerListModel createFromParcel(Parcel in) {
            return new GetCustomerListModel(in);
        }

        @Override
        public GetCustomerListModel[] newArray(int size) {
            return new GetCustomerListModel[size];
        }
    };

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerLocalId() {
        return customerLocalId;
    }

    public void setCustomerLocalId(String customerLocalId) {
        this.customerLocalId = customerLocalId;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerMobileNumber2() {
        return customerMobileNumber2;
    }

    public void setCustomerMobileNumber2(String customerMobileNumber2) {
        this.customerMobileNumber2 = customerMobileNumber2;
    }

    public String getCustomerBankName() {
        return customerBankName;
    }

    public void setCustomerBankName(String customerBankName) {
        this.customerBankName = customerBankName;
    }

    public String getCustomerBankAccountNumber() {
        return customerBankAccountNumber;
    }

    public void setCustomerBankAccountNumber(String customerBankAccountNumber) {
        this.customerBankAccountNumber = customerBankAccountNumber;
    }

    public String getCustomerBankIfccode() {
        return customerBankIfccode;
    }

    public void setCustomerBankIfccode(String customerBankIfccode) {
        this.customerBankIfccode = customerBankIfccode;
    }

    public String getCustomerPaymentMode() {
        return customerPaymentMode;
    }

    public void setCustomerPaymentMode(String customerPaymentMode) {
        this.customerPaymentMode = customerPaymentMode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getInsertedDate() {
        return insertedDate;
    }

    public void setInsertedDate(String insertedDate) {
        this.insertedDate = insertedDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
