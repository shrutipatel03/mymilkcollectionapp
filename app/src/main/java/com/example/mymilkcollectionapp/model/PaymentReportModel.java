package com.example.mymilkcollectionapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentReportModel {
    @SerializedName("me_customer_code")
    @Expose
    private String meCustomerCode;
    @SerializedName("me_customer_name")
    @Expose
    private String meCustomerName;
    @SerializedName("me_weight_type")
    @Expose
    private String meWeightType;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("weight")
    @Expose
    private String weight;

    public String getMeCustomerCode() {
        return meCustomerCode;
    }

    public void setMeCustomerCode(String meCustomerCode) {
        this.meCustomerCode = meCustomerCode;
    }

    public String getMeCustomerName() {
        return meCustomerName;
    }

    public void setMeCustomerName(String meCustomerName) {
        this.meCustomerName = meCustomerName;
    }

    public String getMeWeightType() {
        return meWeightType;
    }

    public void setMeWeightType(String meWeightType) {
        this.meWeightType = meWeightType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
