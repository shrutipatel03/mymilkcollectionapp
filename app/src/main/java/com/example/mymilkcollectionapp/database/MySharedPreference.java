package com.example.mymilkcollectionapp.database;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreference {
    Context Context;
    public String sharedPreferenceName = "milk_collection";
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    public MySharedPreference(android.content.Context context) {
        Context = context;
        sharedPreferences = context.getSharedPreferences(sharedPreferenceName, android.content.Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void userLoginData(String user_id,String user_dairyName,String user_name,String mobileNumber,String email,String password,String dairy_id,String dairy_name,String village,String taluka,String district,String pincode){
        editor.putString("user_id",user_id);
        editor.putString("user_dairyName",user_dairyName);
        editor.putString("user_name",user_name);
        editor.putString("mobile_number",mobileNumber);
        editor.putString("email",email);
        editor.putString("password",password);
        editor.putString("dairy_id",dairy_id);
        editor.putString("dairy_name",dairy_name);
        editor.putString("village",village);
        editor.putString("taluka",taluka);
        editor.putString("district",district);
        editor.putString("pincode",pincode);
        editor.putBoolean("is_login",true);
        editor.commit();
    }

    public String getUserId(){ return sharedPreferences.getString("user_id",""); }
    public String getUserDairyName(){ return sharedPreferences.getString("user_dairyName",""); }
    public String getUserName(){ return sharedPreferences.getString("user_name",""); }
    public String getMobileNumber(){ return sharedPreferences.getString("mobile_number",""); }
    public String getEmail(){ return sharedPreferences.getString("email","");}
    public String getPassword(){ return sharedPreferences.getString("password","");}
    public String getDairyId(){ return sharedPreferences.getString("dairy_id","");}
    public String getDairyName(){ return sharedPreferences.getString("dairy_name","");}
    public String getVillage(){ return sharedPreferences.getString("village","");}
    public String getTaluka(){ return sharedPreferences.getString("taluka","");}
    public String getDistrict(){ return sharedPreferences.getString("district","");}
    public String getPincode(){ return sharedPreferences.getString("pincode",""); }
    public String getLanguage(){ return sharedPreferences.getString("language","en"); }
    public String getPriceMethod(){return sharedPreferences.getString("price","fix");}

    public void setUserId(String user_id){
        editor.putString("user_id",user_id);
        editor.commit();
    }
    public void setCustomerId(String customer_id){
        editor.putString("customer_id",customer_id);
        editor.commit();
    }
    public void setUserDairyName(String user_dairyName){
        editor.putString("user_dairyName",user_dairyName);
        editor.commit();
    }
    public void setUserName(String user_name){
        editor.putString("user_name",user_name);
        editor.commit();
    }
    public void setMobileNumber(String mobileNumber){
        editor.putString("mobile_number",mobileNumber);
        editor.commit();
    }
    public void setEmail(String email){
        editor.putString("email",email);
        editor.commit();
    }
    public void setPassword(String password){
        editor.putString("password",password);
        editor.commit();
    }
    public void setDairyId(String dairy_id){
        editor.putString("dairy_id",dairy_id);
        editor.commit();
    }
    public void setDairyName(String dairyName){
        editor.putString("dairy_name",dairyName);
        editor.commit();
    }
    public void setVillage(String village){
        editor.putString("village",village);
        editor.commit();
    }
    public void setTaluka(String taluka){
        editor.putString("taluka",taluka);
        editor.commit();
    }
    public void setDistrict(String district){
        editor.putString("district",district);
        editor.commit();
    }
    public void setPincode(String pincode){
        editor.putString("pincode",pincode);
        editor.commit();
    }
    public void setLanguage(String language){
        editor.putString("language",language);
        editor.commit();
    }
    public void setPriceMethod(String priceMethod){
        editor.putString("price",priceMethod);
        editor.commit();
    }

    public boolean is_userLogin(){return sharedPreferences.getBoolean("is_login",false); }

    public void is_userLogout(){
       editor.clear();
        editor.commit();
    }
}
