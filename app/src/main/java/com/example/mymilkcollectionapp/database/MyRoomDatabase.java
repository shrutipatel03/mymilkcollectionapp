package com.example.mymilkcollectionapp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.mymilkcollectionapp.roomDatabaseDao.FatandSNFPriceDao;
import com.example.mymilkcollectionapp.roomDatabaseDao.GetUserWisePriceDao;
import com.example.mymilkcollectionapp.roomDatabaseTable.FatandSNFPriceTable;
import com.example.mymilkcollectionapp.roomDatabaseTable.FixPriceTable;
import com.example.mymilkcollectionapp.roomDatabaseDao.FixPriceDao;
import com.example.mymilkcollectionapp.roomDatabaseTable.GetUserWisePriceTable;

@Database(entities = {FixPriceTable.class, FatandSNFPriceTable.class, GetUserWisePriceTable.class},version = 4,exportSchema = false)
public abstract class MyRoomDatabase extends RoomDatabase {

    public abstract FixPriceDao getFixPriceDao();

    public abstract FatandSNFPriceDao getFatandSNFPriceDao();

    public abstract GetUserWisePriceDao getUserWisePriceDao();

    public static MyRoomDatabase getRoomDatabase(Context context){
        return Room.databaseBuilder(context.getApplicationContext(), MyRoomDatabase.class,"milk_collection.db").fallbackToDestructiveMigration().build();
    }
}
