package com.example.mymilkcollectionapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.LoginModel;
import com.example.mymilkcollectionapp.model.LoginResponseModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.google.android.material.snackbar.Snackbar;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class loginActivity extends AppCompatActivity {

    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    MySharedPreference mySharedPreference;
    LoadingView loadingView;
    String mobilePattern = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
        loadingView = new LoadingView(this);
        if(mySharedPreference.is_userLogin()){
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            startActivity(intent);
            finish();

        }
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        getSupportActionBar().setTitle(R.string.Login);
    }

    @OnClick(R.id.btnLogin)
    public void onbtnLogin(View view){
        userLogin();

    }

    @OnClick(R.id.tvRegister)
    public void ontvRegister(){
        Intent intent = new Intent(getApplicationContext(),RegisterActivity.class);
        startActivity(intent);
    }

    public void userLogin(){
        String sMobile = etMobile.getText().toString();
        String sPassword = etPassword.getText().toString();
        if(sMobile.isEmpty()){
            Toast.makeText(getApplicationContext(),"enter mobile number",Toast.LENGTH_LONG).show();
            etMobile.requestFocus();
            return;
        }else if(!sMobile.matches(mobilePattern)){
            Toast.makeText(getApplicationContext(),"enter valid mobile number",Toast.LENGTH_LONG).show();
            etMobile.requestFocus();
            return;
        }
        else if(sPassword.isEmpty()){
            Toast.makeText(getApplicationContext(),"enter password number",Toast.LENGTH_LONG).show();
            etPassword.requestFocus();
            return;
        }else if(sPassword.length()<6){
            Toast.makeText(getApplicationContext(),"Password should be 6 character",Toast.LENGTH_LONG).show();
            etPassword.requestFocus();
            return;
        }

            getDatafromServer(sMobile,sPassword);

    }

    public void getDatafromServer(String mobile,String password){
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponseModel> loginResponseModelCall = apiInterface.getLoginData(mobile,password);
        loginResponseModelCall.enqueue(new Callback<LoginResponseModel>() {
             @Override
             public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                 loadingView.hideDialog();
                LoginResponseModel loginResponseModel = response.body();
                if(loginResponseModel != null){
                    if(loginResponseModel.getResult()){
                        LoginModel data = loginResponseModel.getData();
                        mySharedPreference.userLoginData(data.getUserId(),data.getUserDairyName(),data.getUserName(),data.getUserMobileNumber(),data.getUserEmail(),data.getUserPassword(),data.getDairyId(),data.getDairyName(),data.getVillage(),data.getTaluka(),data.getDistrict(),data.getPincode());

                        Toast.makeText(getApplicationContext(),loginResponseModel.getMsg(),Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }else {
                        if(!mySharedPreference.getMobileNumber().equals(mobile) || !mySharedPreference.getPassword().equals(password)){
                            Snackbar.make(etPassword,"Mobile number or password is wrong",Snackbar.LENGTH_LONG).show();

                        }

                    }
                }
                else {

                    Toast.makeText(getApplicationContext(),loginResponseModel.getMsg(),Toast.LENGTH_LONG).show();
                }
             }

             @Override
             public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                 loadingView.hideDialog();

             }
         });
    }


}
