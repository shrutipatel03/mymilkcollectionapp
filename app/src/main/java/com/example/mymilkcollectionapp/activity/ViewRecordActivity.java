package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.adapter.CustomerListAdapter;
import com.example.mymilkcollectionapp.adapter.ViewRecordAdapter;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.GetCustomerListResponseModel;
import com.example.mymilkcollectionapp.model.GetUserWiseMilkEntryModel;
import com.example.mymilkcollectionapp.model.GetUserWiseMilkEntryResponseModel;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewRecordActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.llselectDate)
    LinearLayout llselectDate;
    @BindView(R.id.tvselectDate)
    TextView tvselectDate;
    @BindView(R.id.spMilkShift)
    Spinner spMilkShift;
    @BindView(R.id.rvUserMilkList)
    RecyclerView rvUSerMilkList;
    @BindView(R.id.llDataNotFound)
    LinearLayout llDataNotFound;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener date;
    SimpleDateFormat simpleDateFormat;
//    String milkshift[] = {"morning","evening"};
    ViewRecordAdapter viewRecordAdapter;
    LoadingView loadingView;
    MySharedPreference mySharedPreference;
    List<GetUserWiseMilkEntryModel> getUserWiseMilkEntryModelList;
    GetUserWiseMilkEntryModel getUserWiseMilkEntryModel;
    SimpleDateFormat serverDateFormat;
    Date selectDate;
    String sspMilkShift;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_record);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.View_Record);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));

        loadingView = new LoadingView(this);
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
        String milkshift[] = {getResources().getString(R.string.morning),getResources().getString(R.string.evening)};
        getUserWiseMilkEntryModelList = new ArrayList<>();
        getUserWiseMilkEntryModel = new GetUserWiseMilkEntryModel();
        spMilkShift.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,milkshift);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMilkShift.setAdapter(arrayAdapter);
        serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()).format(new Date());
        tvselectDate.setText(format);
        calendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateLable();

            }
        };

        viewRecordAdapter =new ViewRecordAdapter(this, getUserWiseMilkEntryModelList, new ViewRecordAdapter.OnItemClickListener() {
            @Override
            public void onItemMilkDataUpdateListener(int position) {
                getUserWiseMilkEntryModel = getUserWiseMilkEntryModelList.get(position);
                Intent intent = new Intent(getApplicationContext(),AddMilkActivity.class);
                intent.putExtra("milk_data",getUserWiseMilkEntryModel);
                startActivityForResult(intent,101);
            }

            @Override
            public void onItemMilkDataDeleteListener(int position) {
                getUserWiseMilkEntryModel = getUserWiseMilkEntryModelList.get(position);
                deleteMilkDatafromServer();
            }
        });
        RecyclerView.LayoutManager layoutManager =new LinearLayoutManager(this);
        rvUSerMilkList.setLayoutManager(layoutManager);
        rvUSerMilkList.setAdapter(viewRecordAdapter);
//        getCustomerDatafromServer(sspMilkShift,serverDateFormat.format(selectDate));
    }

    private void updateLable(){
        String format = "dd-MM-yyyy";
        simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        tvselectDate.setText(simpleDateFormat.format(calendar.getTime()));
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {

         getCustomerDatafromServer(sspMilkShift,serverDateFormat.format(selectDate));

        }
    }

    @OnClick(R.id.llselectDate)
    public void onllselectDate(){
        new DatePickerDialog(ViewRecordActivity.this,date,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick(R.id.btnGo)
    public void onbtnGo(View view){
        sspMilkShift = spMilkShift.getSelectedItem().toString();
        String sSelectDate = tvselectDate.getText().toString();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            selectDate = simpleDateFormat.parse(sSelectDate);
        }catch (ParseException e){

        }
        getCustomerDatafromServer(sspMilkShift,serverDateFormat.format(selectDate));
    }
    public void getCustomerDatafromServer(String shift,String date){
        loadingView.showDialog();
        getUserWiseMilkEntryModelList.clear();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<GetUserWiseMilkEntryResponseModel> getUserWiseMilkEntryResponseModelCall = apiInterface.getUserWiseMilkEntry(mySharedPreference.getUserId(),shift,date);
        getUserWiseMilkEntryResponseModelCall.enqueue(new Callback<GetUserWiseMilkEntryResponseModel>() {
            @Override
            public void onResponse(Call<GetUserWiseMilkEntryResponseModel> call, Response<GetUserWiseMilkEntryResponseModel> response) {
                loadingView.hideDialog();
                GetUserWiseMilkEntryResponseModel getUserWiseMilkEntryResponseModel = response.body();
                if(getUserWiseMilkEntryResponseModel != null){
                    if(getUserWiseMilkEntryResponseModel.getResult()){
                        getUserWiseMilkEntryModelList.addAll(getUserWiseMilkEntryResponseModel.getData());
                        viewRecordAdapter.notifyDataSetChanged();
                        llDataNotFound.setVisibility(View.GONE);
                        rvUSerMilkList.setVisibility(View.VISIBLE);
                    }
                    else {
                        if (getUserWiseMilkEntryModelList.size()<1){
                            llDataNotFound.setVisibility(View.VISIBLE);
                            rvUSerMilkList.setVisibility(View.INVISIBLE);
                        }
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"something wrong",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GetUserWiseMilkEntryResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
    public void deleteMilkDatafromServer(){
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> deleteCustomerDataCall = apiInterface.deleteMilkEntry(getUserWiseMilkEntryModel.getMeId());
        deleteCustomerDataCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel getCustomerListResponseModel = response.body();
                if(getCustomerListResponseModel != null){
                    if(getCustomerListResponseModel.getResult()){
                        getCustomerDatafromServer(sspMilkShift,serverDateFormat.format(selectDate));
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"something wrong",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"something wrong",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
