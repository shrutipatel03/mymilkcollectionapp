package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.FatWisePriceModel;
import com.example.mymilkcollectionapp.model.FatWisePriceResponseModel;
import com.example.mymilkcollectionapp.model.GetCustomerListResponseModel;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FatWisePriceActivity extends AppCompatActivity {

    @BindView(R.id.llContainer)
    LinearLayout llContainer;
    String milkType;
    LoadingView loadingView;
    MySharedPreference mySharedPreference;
    View view;
    List<View> viewList;
    List<FatWisePriceModel> fatWisePriceModelList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fat_wise_price);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.Fat_Wise_Price);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        milkType = getIntent().getStringExtra("milk_type");
        loadingView = new LoadingView(this);
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
        viewList = new ArrayList<>();
        fatWisePriceModelList = new ArrayList<>();
        getFatWiseDatafromServer();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnFatWisePriceUpdate)
    public void onbtnFatWisePriceUpdate(){
        if(viewList.isEmpty()){
            Toast.makeText(getApplicationContext(),"Data Not found",Toast.LENGTH_LONG).show();
            return;
        }else {
            int position = 0;
            for (View view:viewList){
                TextView tvFat = view.findViewById(R.id.tvFat);
                EditText etRate = view.findViewById(R.id.etRate);
                String sRate = etRate.getText().toString();
                if (sRate.trim().length() == 0){
                    Toast.makeText(getApplicationContext(),"Data Not found",Toast.LENGTH_LONG).show();
                    return;
                }else {
                    FatWisePriceModel fatWisePriceModel = fatWisePriceModelList.get(position);
                    fatWisePriceModel.setFpRate(sRate);
                }position++;
            }

            Gson  gson = new GsonBuilder().create();
            JsonArray jsonArray = gson.toJsonTree(fatWisePriceModelList).getAsJsonArray();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("result",true);
            jsonObject.add("data",jsonArray);
            sendUpdateRate(jsonObject);
        }
    }
    public void getFatWiseDatafromServer(){

        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FatWisePriceResponseModel> fatWisePriceResponseModelCall = apiInterface.getFatWisePrice(mySharedPreference.getUserId(),"fat",milkType,"0");
        fatWisePriceResponseModelCall.enqueue(new Callback<FatWisePriceResponseModel>() {
            @Override
            public void onResponse(Call<FatWisePriceResponseModel> call, Response<FatWisePriceResponseModel> response) {
                loadingView.hideDialog();
                FatWisePriceResponseModel fatWisePriceResponseModel = response.body();
                if(fatWisePriceResponseModel != null){
                    if(fatWisePriceResponseModel.getResult()){
                     for (FatWisePriceModel fatWisePriceModel:fatWisePriceResponseModel.getData()){
                         view = getLayoutInflater().inflate(R.layout.fatwise_price_inflater,null);
                         TextView tvFat = view.findViewById(R.id.tvFat);
                         EditText etRate = view.findViewById(R.id.etRate);
                         tvFat.setText(fatWisePriceModel.getFsValue());
                         etRate.setText(fatWisePriceModel.getFpRate());
                         Log.d("FatWisePriceActivity","rate "+etRate);
                         llContainer.addView(view);
                         viewList.add(view);
                     }

                        fatWisePriceModelList.addAll(fatWisePriceResponseModel.getData());
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FatWisePriceResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }

    public void sendUpdateRate(JsonObject object){
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> statusModelCall = apiInterface.updateAdminFatRate(object);
        statusModelCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel statusModel = response.body();
                if(statusModel != null){
                    if(statusModel.getResult()){
                        Toast.makeText(getApplicationContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
