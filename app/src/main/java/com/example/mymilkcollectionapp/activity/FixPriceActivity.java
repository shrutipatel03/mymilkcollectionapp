package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.FatWisePriceModel;
import com.example.mymilkcollectionapp.model.FatWisePriceResponseModel;
import com.example.mymilkcollectionapp.model.GetFixPriceModel;
import com.example.mymilkcollectionapp.model.GetFixPriceResponseModel;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FixPriceActivity extends AppCompatActivity {

    LoadingView loadingView;
    MySharedPreference mySharedPreference;
    @BindView(R.id.etFixPrice)
    EditText etFixPrice;
    String milkType;
    String fat_wise_price;
    private static final String TAG = "FixPriceActivity" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fix_price);
        ButterKnife.bind(this);
        initAllControls();
    }
    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.Fix_Price);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        Intent intent = getIntent();
        milkType = intent.getStringExtra("milk_type");
        Log.d(TAG,"milk"+ milkType);
        loadingView = new LoadingView(this);
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
        getFixPriceDataFromServer();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnFixPriceUpdate)
    public void onbtnFixPriceUpdate(){
        String totalPrice = etFixPrice.getText().toString();
        if (totalPrice.isEmpty()){
            Snackbar.make(etFixPrice,"fix price is empty",Snackbar.LENGTH_LONG).show();
            return;
        }
        fat_wise_price = String.valueOf(Double.parseDouble(totalPrice)/100);
        updateFixPriceDataFromServer(totalPrice,fat_wise_price);
    }

    public void getFixPriceDataFromServer(){
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<GetFixPriceResponseModel> getFixPriceResponseModelCall = apiInterface.getFixPrice(mySharedPreference.getUserId());
        getFixPriceResponseModelCall.enqueue(new Callback<GetFixPriceResponseModel>() {


            @Override
            public void onResponse(Call<GetFixPriceResponseModel> call, Response<GetFixPriceResponseModel> response) {
                loadingView.hideDialog();
                GetFixPriceResponseModel getFixPriceResponseModel = response.body();
                if(getFixPriceResponseModel != null){
                    if(getFixPriceResponseModel.getResult()){
                        for (GetFixPriceModel getFixPriceModel : getFixPriceResponseModel.getData()){
                            Log.d(TAG,"fixPrice"+getFixPriceModel.getFwpTotalKgFatPrice());
                            if(getFixPriceModel.getFwpMilkType().equalsIgnoreCase(milkType)){
                                etFixPrice.setText(getFixPriceModel.getFwpTotalKgFatPrice());
                            }
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<GetFixPriceResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
    public void updateFixPriceDataFromServer(String totalPrice,String fatWisePrice){
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> updateFixPriceResponseModelCall = apiInterface.updateFixPrice(mySharedPreference.getUserId(),milkType,totalPrice,fatWisePrice);
        updateFixPriceResponseModelCall.enqueue(new Callback<StatusModel>() {


            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel updateFixPriceResponseModel = response.body();
                if(updateFixPriceResponseModel != null){
                    if(updateFixPriceResponseModel.getResult()){
                        Toast.makeText(getApplicationContext(),updateFixPriceResponseModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),updateFixPriceResponseModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
