package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.adapter.MyTabAdapter;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.fragment.FatandSnfWisePriceFragment;
import com.example.mymilkcollectionapp.model.FatWisePriceModel;
import com.example.mymilkcollectionapp.model.FatWisePriceResponseModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FatAndSnfWisePriceActivity extends AppCompatActivity {

    LoadingView loadingView;
    MyTabAdapter myTabAdapter;
    String milkType;
    MySharedPreference mySharedPreference;
    @BindView(R.id.vpFatandSnfWisePrice)
    ViewPager vpFatandSnfWisePrice;
    @BindView(R.id.tlFatandSnfWisePrice)
    TabLayout tlFatandSnfWisePrice;
    private String TAG = "FatAndSnfWisePriceActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fat_and_snf_wise_price);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.Fat_And_SNF_Wise_Price);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));

        milkType = getIntent().getStringExtra("milk_type");

        loadingView = new LoadingView(this);
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
        myTabAdapter = new MyTabAdapter(getSupportFragmentManager());
        tlFatandSnfWisePrice.setupWithViewPager(vpFatandSnfWisePrice);
        vpFatandSnfWisePrice.setAdapter(myTabAdapter);

        getFatWiseDatafromServer();
    }

    public void getFatWiseDatafromServer() {

        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FatWisePriceResponseModel> fatWisePriceResponseModelCall = apiInterface.getFatWisePrice(mySharedPreference.getUserId(), "snf", milkType, "0");
        fatWisePriceResponseModelCall.enqueue(new Callback<FatWisePriceResponseModel>() {
            @Override
            public void onResponse(Call<FatWisePriceResponseModel> call, Response<FatWisePriceResponseModel> response) {
                loadingView.hideDialog();
                FatWisePriceResponseModel fatWisePriceResponseModel = response.body();
                if (fatWisePriceResponseModel != null) {
                    if (fatWisePriceResponseModel.getResult()) {
                        for (FatWisePriceModel fatWisePriceModel : fatWisePriceResponseModel.getData()) {
                            Bundle bundle = new Bundle();
                            bundle.putString("milk_data", milkType);
                            bundle.putParcelable("data", fatWisePriceModel);
                            FatandSnfWisePriceFragment fatandSnfWisePriceFragment = new FatandSnfWisePriceFragment();
                            fatandSnfWisePriceFragment.setArguments(bundle);
                            myTabAdapter.addFragment(fatandSnfWisePriceFragment, fatWisePriceModel.getFsValue());

                            Log.d(TAG, "title" + fatWisePriceModel.getFsValue());
                  }

                        myTabAdapter.notifyDataSetChanged();


                    } else {
                        Toast.makeText(getApplicationContext(), "data found", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "data found", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FatWisePriceResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
