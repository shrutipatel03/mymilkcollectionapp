package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.adapter.PaymentReportAdapter;
import com.example.mymilkcollectionapp.adapter.ViewRecordAdapter;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.GetUserWiseMilkEntryModel;
import com.example.mymilkcollectionapp.model.GetUserWiseMilkEntryResponseModel;
import com.example.mymilkcollectionapp.model.PaymentReportModel;
import com.example.mymilkcollectionapp.model.PaymentReportResponseModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.uttampanchasara.pdfgenerator.CreatePdf;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentReportActivity extends AppCompatActivity {

    @BindView(R.id.tvFromDate)
    TextView tvFromDate;
    @BindView(R.id.tvToDate)
    TextView tvToDate;
    @BindView(R.id.rvPaymentReport)
    RecyclerView rvPaymentReport;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    @BindView(R.id.tvTotalRs)
    TextView tvTotalRs;
    @BindView(R.id.llDataNotFound)
    LinearLayout llDataNotFound;
    MySharedPreference mySharedPreference;
    Calendar fromDatecalendar;
    Calendar toDateCalendar;
    DatePickerDialog.OnDateSetListener datePickerfromdate;
    DatePickerDialog.OnDateSetListener datePickertodate;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat serverDateFormat;
    LoadingView loadingView;
    Date from_start_Date,to_end_Date;
    List<PaymentReportModel> paymentReportModelList;
    PaymentReportAdapter paymentReportAdapter;
    private MenuItem item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_report);
        ButterKnife.bind(this);
        initAllControls();
    }


    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.payment_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        mySharedPreference = new MySharedPreference(this);
        loadingView = new LoadingView(this);

        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);

        serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()).format(new Date());
        tvFromDate.setText(format);
        tvToDate.setText(format);
        fromDatecalendar = Calendar.getInstance();
        datePickerfromdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                fromDatecalendar.set(Calendar.YEAR,year);
                fromDatecalendar.set(Calendar.MONTH,month);
                fromDatecalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateLable();
            }

        };
        toDateCalendar  = Calendar.getInstance();
        datePickertodate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                toDateCalendar.set(Calendar.YEAR,year);
                toDateCalendar.set(Calendar.MONTH,month);
                toDateCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateLable();
            }

        };

        paymentReportModelList = new ArrayList<>();

        paymentReportAdapter =new PaymentReportAdapter(this, paymentReportModelList);

        RecyclerView.LayoutManager layoutManager =new LinearLayoutManager(this);
        rvPaymentReport.setLayoutManager(layoutManager);
        rvPaymentReport.setAdapter(paymentReportAdapter);

    }
    @OnClick(R.id.btnGo)
    public void onbtnGo(View view){
        String fromDate = tvFromDate.getText().toString();
        String toDate = tvToDate.getText().toString();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            from_start_Date = simpleDateFormat.parse(fromDate);
            to_end_Date = simpleDateFormat.parse(toDate);
        }catch (ParseException e){

        }
        item.setVisible(false);
        getPaymentReportDatafromServer(serverDateFormat.format(from_start_Date),serverDateFormat.format(to_end_Date));

    }


    @OnClick(R.id.llSelectFromDate)
    public void onivSelectFromDate(View view){
        new DatePickerDialog(PaymentReportActivity.this,datePickerfromdate,fromDatecalendar.get(Calendar.YEAR),fromDatecalendar.get(Calendar.MONTH),fromDatecalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    @OnClick(R.id.llSelectToDate)
    public void onivSelectToDate(View view){
        new DatePickerDialog(PaymentReportActivity.this,datePickertodate, toDateCalendar.get(Calendar.YEAR), toDateCalendar.get(Calendar.MONTH), toDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_download,menu);
        item = menu.findItem(R.id.download_report);
        item.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }if(item.getItemId() == R.id.download_report){
            if (paymentReportModelList.size()>0) {

                Dexter.withActivity(this)
                        .withPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        /* ... */
                        if (report.areAllPermissionsGranted()) {
                            DownloadPdf();
                            Toast.makeText(getApplicationContext(), "Download", Toast.LENGTH_LONG).show();
                        } else {
                            showPermissionDailog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
            }else {
                Toast.makeText(getApplicationContext(), "No data found", Toast.LENGTH_LONG).show();
            }
        }
        return true;
    }
    private void DownloadPdf() {
        File directory = createDirectory();
        String content = "<html>" +
                "    <body>" +
                "        <table width='100%' >" +
                "            <tbody>" +
                "                <tr>" +
                "                    <th colspan='3'><h1>" + mySharedPreference.getUserDairyName() + "</h1></th>" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" +
                "                        " + getString(R.string.from_Date) + " :- " + simpleDateFormat.format(from_start_Date) +
                "                    </td>" +
                "                    <td align='center'>" +
                "                        " + getString(R.string.to_Date) + " :- " + simpleDateFormat.format(to_end_Date) +
                "                    </td>" +
                "                    <td align='right'>" +
                "                        " + getString(R.string.date) + " :- " + simpleDateFormat.format(new Date()) +
                "                    </td>" +
                "                </tr>" +
                "            </tbody>" +
                "        </table>" +
                "        <hr>" +
                "        <table border='1' cellspacing='0' align='center' width='100%'>" +
                "            <tbody>" +
                "                <tr>" +
                "                    <th colspan='6'><font size='5'>"+getResources().getString(R.string.payment_report)+"</font></th>" +
                "                </tr>" +
                "                <tr>" +
                "                    <th align='center'>" + getString(R.string.customer_code) + "</th>" +
                "                    <th align='center'>" + getString(R.string.Customer_Name) + "</th>" +
//                "                    <th>" + getString(R.string.mobile_number) + "</th>" +
                "                    <th align='center'>" + getString(R.string.total_milk_ltr) + "</th>" +
                "                    <th align='center'>" + getString(R.string.total_rs) + "</th>" +
                "                    <th align='center'>" + getString(R.string.sign) + "</th>" +
                "                </tr>";
        for (PaymentReportModel paymentReportLocal : paymentReportModelList) {
            content += "                <tr>" +
                    "                    <td align='center'>" + paymentReportLocal.getMeCustomerCode() + "</td>" +
                    "                    <td align='center'>" + paymentReportLocal.getMeCustomerName() + "</td>" +
//                    "                    <td>" + paymentReportLocal.getMe_Customer_Name() + "</td>" +
                    "                    <td align='center'>" + paymentReportLocal.getWeight() + " " + paymentReportLocal.getMeWeightType() + "</td>" +
                    "                    <td align='center'>" + paymentReportLocal.getTotalAmount() + "</td>" +
                    "                    <td align='center'>" + "  " + "</td>" +
                    "                </tr>";
        }

        content += "            </tbody>" +
                "        </table>" +
                "    </body>" +
                "</html>";
        new CreatePdf(getApplicationContext())
                .setPdfName("Payment_report_"+simpleDateFormat.format(from_start_Date)+"_"+simpleDateFormat.format(to_end_Date))
                .openPrintDialog(false)
                .setContentBaseUrl(null)
                .setContent(content)
                .setFilePath(directory.getAbsolutePath() + File.separator)
                .setCallbackListener(new CreatePdf.PdfCallbackListener() {
                    @Override
                    public void onFailure( String s) {
                        Toast.makeText(getApplicationContext(), "download failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess( String s) {
                        String file_path=directory.getAbsolutePath() + File.separator+"Payment_report_"+simpleDateFormat.format(from_start_Date)+"_"+simpleDateFormat.format(to_end_Date);
                        Toast.makeText(getApplicationContext(), "complete download"+file_path, Toast.LENGTH_SHORT).show();
                    }
                })
                .create();

    }

    private File createDirectory() {
        File mFile;
        if (isExternalStorageWritable()) {
            mFile = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "milk_collection");
        } else {
            mFile = new File(Environment.getDataDirectory().toString() + File.separator + "milk_collection");
        }

        if (!mFile.exists())
            mFile.mkdirs();
        return mFile;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void showPermissionDailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentReportActivity.this);
        builder.setTitle("Need Send SMS Permission");
        builder.setMessage("This app needs SMS permission for send please give permission from settings.");
        builder.setPositiveButton("Go To Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void updateLable() {
        String format = "dd-MM-yyyy";
        simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        tvFromDate.setText(simpleDateFormat.format(fromDatecalendar.getTime()));
        tvToDate.setText(simpleDateFormat.format(toDateCalendar.getTime()));

    }

    public void getPaymentReportDatafromServer(String start_date ,String end_date){
        loadingView.showDialog();
        paymentReportModelList.clear();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentReportResponseModel> paymentReportResponseModelCall = apiInterface.getPaymentReport(start_date,end_date,mySharedPreference.getUserId());
        paymentReportResponseModelCall.enqueue(new Callback<PaymentReportResponseModel>() {
            @Override
            public void onResponse(Call<PaymentReportResponseModel> call, Response<PaymentReportResponseModel> response) {
                loadingView.hideDialog();
                PaymentReportResponseModel paymentReportResponseModel = response.body();
                if(paymentReportResponseModel != null){
                   item.setVisible(true);
                    if(paymentReportResponseModel.getResult()){

                            paymentReportModelList.addAll(paymentReportResponseModel.getData());
                            paymentReportAdapter.notifyDataSetChanged();
                            llDataNotFound.setVisibility(View.GONE);
                            rvPaymentReport.setVisibility(View.VISIBLE);


                            int total_weight = 0;
                            int total_Amount = 0;
                            for (PaymentReportModel paymentReportModel : paymentReportModelList) {
                                total_weight += Double.parseDouble(paymentReportModel.getWeight());
                                total_Amount += Double.parseDouble(paymentReportModel.getTotalAmount());
                            }
                            tvTotal.setText("" + total_weight);
                            tvTotalRs.setText("" + total_Amount);
                            if (paymentReportModelList.size()<1){
                                llDataNotFound.setVisibility(View.VISIBLE);
                                rvPaymentReport.setVisibility(View.INVISIBLE);
                                item.setVisible(false);
                            }

                    }
                    else {

                        Toast.makeText(getApplicationContext(),"Data not Found",Toast.LENGTH_LONG).show();

                    }
                }else {
                    item.setVisible(false);
                    Toast.makeText(getApplicationContext(),"Data not Found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentReportResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
