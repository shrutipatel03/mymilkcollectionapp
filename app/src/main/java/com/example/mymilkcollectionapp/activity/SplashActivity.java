package com.example.mymilkcollectionapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.model.FatWisePriceModel;
import com.example.mymilkcollectionapp.model.FatWisePriceResponseModel;
import com.example.mymilkcollectionapp.model.GetUserWisePriceModel;
import com.example.mymilkcollectionapp.model.GetUserWisePriceResponceModel;
import com.example.mymilkcollectionapp.model.SNFFatAllListModel;
import com.example.mymilkcollectionapp.model.SNFFatAllListResponceModel;
import com.example.mymilkcollectionapp.roomDatabaseTable.FatandSNFPriceTable;
import com.example.mymilkcollectionapp.roomDatabaseTable.FixPriceTable;
import com.example.mymilkcollectionapp.database.MyRoomDatabase;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.GetFixPriceModel;
import com.example.mymilkcollectionapp.model.GetFixPriceResponseModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.example.mymilkcollectionapp.roomDatabaseTable.GetUserWisePriceTable;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_SCREEN_TIME_OUT=2000;
    LoadingView loadingView;
    MySharedPreference mySharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    public void init(){
        loadingView = new LoadingView(this);
        mySharedPreference = new MySharedPreference(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();


            }
        }, SPLASH_SCREEN_TIME_OUT);
        deleteFixPrice();
        deleteFatandSNFPrice();
        deleteUserWisePrice();
    }
    public void getFixPriceDataFromServer(){

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<GetFixPriceResponseModel> getFixPriceResponseModelCall = apiInterface.getFixPrice(mySharedPreference.getUserId());
        getFixPriceResponseModelCall.enqueue(new Callback<GetFixPriceResponseModel>() {


            @Override
            public void onResponse(Call<GetFixPriceResponseModel> call, Response<GetFixPriceResponseModel> response) {

                GetFixPriceResponseModel getFixPriceResponseModel = response.body();
                if(getFixPriceResponseModel != null){
                    if(getFixPriceResponseModel.getResult()){
                        List<FixPriceTable> fixPriceTableList = new ArrayList<>();
                         for (GetFixPriceModel getFixPriceModel : getFixPriceResponseModel.getData()){
                             FixPriceTable fixPriceTable = new FixPriceTable();
                             fixPriceTable.setFwpId(getFixPriceModel.getFwpId());
                             fixPriceTable.setFwpTotalKgFatPrice(getFixPriceModel.getFwpTotalKgFatPrice());
                             fixPriceTable.setFwpFatWisePrice(getFixPriceModel.getFwpFatWisePrice());
                             fixPriceTable.setFwpMilkType(getFixPriceModel.getFwpMilkType());
                             fixPriceTable.setFwpUserId(getFixPriceModel.getFwpUserId());
                             fixPriceTable.setFwpInsertedDate(getFixPriceModel.getFwpInsertedDate());
                             fixPriceTable.setFwpUpdateDate(getFixPriceModel.getFwpUpdateDate());
                             fixPriceTableList.add(fixPriceTable);
                         }
                         if(fixPriceTableList.size()>0){
                             insertFixPrice(fixPriceTableList);
                         }

                    }
                    else {
                        Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GetFixPriceResponseModel> call, Throwable t) {
            }
        });
    }


    private void insertFixPrice(List<FixPriceTable> fixPriceTableList) {


        InsertFixPrice insertFixPrice = new InsertFixPrice(fixPriceTableList);
        insertFixPrice.execute();

    }

    class InsertFixPrice extends AsyncTask<Void,Void,Long>{
        List<FixPriceTable> fixPriceTableList;

        public InsertFixPrice(List<FixPriceTable> fixPriceTableList) {
            this.fixPriceTableList = fixPriceTableList;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            long data[] = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getFixPriceDao().insertMultipleData(fixPriceTableList);
            Log.d("SplashActivity","fix price data "+data.length);
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }


    public void getFatandSNFWiseDatafromServer() {

//        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SNFFatAllListResponceModel> fatandSNFWisePriceResponseModelCall = apiInterface.getAllSnfAndFat();
        fatandSNFWisePriceResponseModelCall.enqueue(new Callback<SNFFatAllListResponceModel>() {
            @Override
            public void onResponse(Call<SNFFatAllListResponceModel> call, Response<SNFFatAllListResponceModel> response) {
//                loadingView.hideDialog();
                SNFFatAllListResponceModel fatandSNFWisePriceResponseModel = response.body();
                if (fatandSNFWisePriceResponseModel != null) {
                    if (fatandSNFWisePriceResponseModel.getResult()) {
                        List<FatandSNFPriceTable> fatandSNFPriceTableList = new ArrayList<>();
                        for (SNFFatAllListModel fatandSNFWisePriceModel : fatandSNFWisePriceResponseModel.getData()) {
                           FatandSNFPriceTable fatandSNFPriceTable = new FatandSNFPriceTable();
                           fatandSNFPriceTable.setFsId(fatandSNFWisePriceModel.getFsId());
                           fatandSNFPriceTable.setFsType(fatandSNFWisePriceModel.getFsType());
                           fatandSNFPriceTable.setFsValue(fatandSNFWisePriceModel.getFsValue());
                           fatandSNFPriceTable.setFsMilkType(fatandSNFWisePriceModel.getFsMilkType());
                           fatandSNFPriceTable.setFsParentId(fatandSNFWisePriceModel.getFsParentId());
                           fatandSNFPriceTable.setFsInsertDate(fatandSNFWisePriceModel.getFsInsertDate());
                           fatandSNFPriceTable.setFsUpdateDate(fatandSNFWisePriceModel.getFsUpdateDate());
                           fatandSNFPriceTableList.add(fatandSNFPriceTable);
                        }
                        if (fatandSNFPriceTableList.size()>0){
                            insertFatandSNFPrice(fatandSNFPriceTableList);
                        }



                    } else {
                        Toast.makeText(getApplicationContext(), "data found", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "data found", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<SNFFatAllListResponceModel> call, Throwable t) {
//                loadingView.hideDialog();
            }
        });
    }

    public void insertFatandSNFPrice(List<FatandSNFPriceTable> fatandSNFPriceTableList){
        InsertFatandSNFPrice insertFatandSNFPrice = new InsertFatandSNFPrice(fatandSNFPriceTableList);
        insertFatandSNFPrice.execute();
    }

    class InsertFatandSNFPrice extends AsyncTask<Void,Void,Long>{
        List<FatandSNFPriceTable> fatandSNFPriceTableList;

        public InsertFatandSNFPrice(List<FatandSNFPriceTable> fatandSNFPriceTableList) {
            this.fatandSNFPriceTableList = fatandSNFPriceTableList;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            long[] data = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getFatandSNFPriceDao().insertMultipleData(fatandSNFPriceTableList);
            Log.d("SplashActivity","fat and snf data "+data.length);
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }

    public void getFatWiseDatafromServer(){
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<GetUserWisePriceResponceModel> getUserWisePriceResponceModelCall = apiInterface.getUserWisePrice(mySharedPreference.getUserId());
        getUserWisePriceResponceModelCall.enqueue(new Callback<GetUserWisePriceResponceModel>() {
            @Override
            public void onResponse(Call<GetUserWisePriceResponceModel> call, Response<GetUserWisePriceResponceModel> response) {
                GetUserWisePriceResponceModel getUserWisePriceResponceModel = response.body();
                if(getUserWisePriceResponceModel != null){
                    if(getUserWisePriceResponceModel.getResult()){
                        List<GetUserWisePriceTable> getUserWisePriceTableList = new ArrayList<>();
                        for (GetUserWisePriceModel getUserWisePriceModel:getUserWisePriceResponceModel.getData()){
                            GetUserWisePriceTable getUserWisePriceTable = new GetUserWisePriceTable();
                            getUserWisePriceTable.setFpFsId(getUserWisePriceModel.getFpFsId());
                            getUserWisePriceTable.setFpId(getUserWisePriceModel.getFpId());
                            getUserWisePriceTable.setFpUId(getUserWisePriceModel.getFpUId());
                            getUserWisePriceTable.setFpRate(getUserWisePriceModel.getFpRate());
                            getUserWisePriceTable.setFpInsertedDate(getUserWisePriceModel.getFpInsertedDate());
                            getUserWisePriceTable.setFpUpdatedDate(getUserWisePriceModel.getFpUpdatedDate());
                            getUserWisePriceTableList.add(getUserWisePriceTable);
                        }
                        if(getUserWisePriceTableList.size()>0){
                            insertuserWisePrice(getUserWisePriceTableList);
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GetUserWisePriceResponceModel> call, Throwable t) {
            }
        });
    }
    public void insertuserWisePrice(List<GetUserWisePriceTable> getUserWisePriceTableList){
        InsertUSerWisePrice insertUSerWisePrice = new InsertUSerWisePrice(getUserWisePriceTableList);
        insertUSerWisePrice.execute();
    }

    class InsertUSerWisePrice extends AsyncTask<Void,Void,Long>{
        List<GetUserWisePriceTable> getUserWisePriceTableList;

        public InsertUSerWisePrice(List<GetUserWisePriceTable> getUserWisePriceTableList) {
            this.getUserWisePriceTableList = getUserWisePriceTableList;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            long[] data = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getUserWisePriceDao().insertMultipleData(getUserWisePriceTableList);
            Log.d("SplashActivity","user price data "+data.length);
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }
    private void deleteFixPrice(){

        DeleteFixPrice deleteFixPrice = new DeleteFixPrice();
        deleteFixPrice.execute();
    }
    class DeleteFixPrice extends AsyncTask<Void,Void,Integer>{

        @Override
        protected Integer doInBackground(Void... voids) {
            int data = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getFixPriceDao().DeleteAll();
            Log.d("SplashActivity","delete data "+data);
            return data;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            getFixPriceDataFromServer();
        }
    }
    private void deleteFatandSNFPrice(){

        DeleteFatandSNFPrice deleteFatandSNFPrice = new DeleteFatandSNFPrice();
        deleteFatandSNFPrice.execute();
    }
    class DeleteFatandSNFPrice extends AsyncTask<Void,Void,Integer>{

        @Override
        protected Integer doInBackground(Void... voids) {
            int data = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getFatandSNFPriceDao().deleteAll();
            Log.d("SplashActivity","delete data "+data);
            return data;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            getFatandSNFWiseDatafromServer();
        }
    }
    private void deleteUserWisePrice(){

        DeleteUserWisePrice deleteUserWisePrice = new DeleteUserWisePrice();
        deleteUserWisePrice.execute();
    }
    class DeleteUserWisePrice extends AsyncTask<Void,Void,Integer>{

        @Override
        protected Integer doInBackground(Void... voids) {
            int data = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getUserWisePriceDao().deleteAll();
            Log.d("SplashActivity","delete data "+data);
            return data;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            getFatWiseDatafromServer();
        }
    }

}
