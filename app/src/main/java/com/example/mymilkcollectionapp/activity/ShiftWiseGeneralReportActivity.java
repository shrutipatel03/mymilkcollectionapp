package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.ShiftWiseGeneralReportModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.uttampanchasara.pdfgenerator.CreatePdf;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class ShiftWiseGeneralReportActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{


    @BindView(R.id.llselectDate)
    LinearLayout llselectDate;
    @BindView(R.id.spMilkShift)
    Spinner spMilkShift;
    @BindView(R.id.tvselectDate)
    TextView tvselectDate;
    @BindView(R.id.tvTotalMilkltr)
    TextView tvTotalMilkltr;
    @BindView(R.id.tvTotalRs)
    TextView tvTotalRs;
    @BindView(R.id.tvTotalAvFat)
    TextView tvTotalAvFat;
    @BindView(R.id.tvBuffaloMilkltr)
    TextView tvBuffaloMilkltr;
    @BindView(R.id.tvBuffaloTotalRs)
    TextView tvBuffaloTotalRs;
    @BindView(R.id.tvBuffaloAvFat)
    TextView tvBuffaloAvFat;
    @BindView(R.id.tvCowMilkltr)
    TextView tvCowMilkltr;
    @BindView(R.id.tvCowTotalRs)
    TextView tvCowTotalRs;
    @BindView(R.id.tvCowAvFat)
    TextView tvCowAvFat;
    @BindView(R.id.llDataNotFound)
    LinearLayout llDataNotFound;
    @BindView(R.id.svShiftWiseGeneralReport)
    ScrollView svShiftWiseGeneralReport;
    MySharedPreference mySharedPreference;
    LoadingView loadingView;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener date;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat serverDateFormat;
    Date selectDate;
    String sspMilkShift;
    private MenuItem item;
    ShiftWiseGeneralReportModel shiftWiseGeneralReportModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shift_wise_general_report);
        ButterKnife.bind(this);
        initAllControls();
    }
    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.shift_wise_general_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
        loadingView = new LoadingView(this);

        String milkshift[] = {getResources().getString(R.string.morning),getResources().getString(R.string.evening)};
        spMilkShift.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,milkshift);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMilkShift.setAdapter(arrayAdapter);

        serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()).format(new Date());
        tvselectDate.setText(format);
        calendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateLable();

            }
        };

    }

    private void updateLable(){
        String format = "dd-MM-yyyy";
        simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        tvselectDate.setText(simpleDateFormat.format(calendar.getTime()));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_download,menu);
        item = menu.findItem(R.id.download_report);
        item.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        if(item.getItemId() == R.id.download_report){

            if (shiftWiseGeneralReportModel != null) {
                Dexter.withActivity(this)
                        .withPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        /* ... */
                        if (report.areAllPermissionsGranted()) {
                            DownloadPdf();
                            Toast.makeText(getApplicationContext(), "Download", Toast.LENGTH_LONG).show();
                        } else {
                            showPermissionDailog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
            } else {
                Toast.makeText(getApplicationContext(), "no data found", Toast.LENGTH_LONG).show();
            }
        }
        return true;
    }
    private void DownloadPdf() {
        File directory = createDirectory();

        String content = "<html>" +
                "    <body>" +
                "        <table width='100%' >" +
                "            <tbody>" +
                "                <tr>" +
                "                    <th colspan='2'><h1>" + mySharedPreference.getUserDairyName() + "</h1></th>\n" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" +
                "                        " + getString(R.string.shift) + " :- " + shiftWiseGeneralReportModel.getShift() +
                "                    </td>" +
                "                    <td align='right'>" +
                "                        " + getString(R.string.date) + " :- " + shiftWiseGeneralReportModel.getDate() +
                "                    </td>" +
                "                </tr>" +
                "            </tbody>" +
                "        </table>" +
                "        <hr>" +
                "        <table border='1' cellspacing='0' align='center' width='50%'>" +
                "            <tbody>" +
                "                <tr>" +
                "                    <th colspan='2'>"+getResources().getString(R.string.shift_wise_general_report)+"</th>" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" + getString(R.string.total_milk_ltr) + "</td>" +
                "                    <td align='center'>" + shiftWiseGeneralReportModel.getTotalMilk() + "</td>" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" + getString(R.string.total_rs) + "</td>" +
                "                    <td align='center'>" + shiftWiseGeneralReportModel.getTotalRs() + "</td>" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" + getString(R.string.total_av_fat) + "</td>" +
                "                    <td align='center'>" + shiftWiseGeneralReportModel.getTotalAvgFat() + "</td>" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" + getString(R.string.Buffalo_milk_ltr) + "</td>" +
                "                    <td align='center'>" + shiftWiseGeneralReportModel.getBufMilkLtr() + "</td>" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" + getString(R.string.cow_milk_ltr) + "</td>" +
                "                    <td align='center'>" + shiftWiseGeneralReportModel.getCowMilkLtr() + "</td>" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" + getString(R.string.Cow_total_rs) + "</td>" +
                "                    <td align='center'>" + shiftWiseGeneralReportModel.getCowRs() + "</td>" +
                "                </tr>" +
                "                <tr>" +
                "                    <td>" + getString(R.string.Cow_av_fat) + "</td>" +
                "                    <td align='center'>" + shiftWiseGeneralReportModel.getCowAvgFat() + "</td>" +
                "                </tr>" +
                "            </tbody>" +
                "        </table>" +
                "    </body>" +
                "</html>";
        new CreatePdf(getApplicationContext())
                .setPdfName("General_report_" + date + "_" + sspMilkShift)
                .openPrintDialog(false)
                .setContentBaseUrl(null)
                .setContent(content)
                .setFilePath(directory.getAbsolutePath() + File.separator)
                .setCallbackListener(new CreatePdf.PdfCallbackListener() {
                    @Override
                    public void onFailure(String s) {
                        Toast.makeText(getApplicationContext(),"failed download", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(String s) {
                        String file_path=directory.getAbsolutePath() + File.separator+"General_report_" + date + "_" + sspMilkShift;
                        Toast.makeText(getApplicationContext(), "download complete "+file_path, Toast.LENGTH_SHORT).show();
                    }
                })
                .create();

    }

    private File createDirectory() {
        File mFile;
        if (isExternalStorageWritable()) {
            mFile = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "milk_collection");
        } else {
            mFile = new File(Environment.getDataDirectory().toString() + File.separator + "milk_collection");
        }

        if (!mFile.exists())
            mFile.mkdirs();
        return mFile;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void showPermissionDailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Need Send SMS Permission");
        builder.setMessage("This app needs SMS permission for send please give permission from settings.");
        builder.setPositiveButton("Go To Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick(R.id.llselectDate)
    public void onllselectDate(){
        new DatePickerDialog(ShiftWiseGeneralReportActivity.this,date,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    @OnClick(R.id.btnGo)
    public void onbtnGo(View view){
        sspMilkShift = spMilkShift.getSelectedItem().toString();
        String sSelectDate = tvselectDate.getText().toString();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            selectDate = simpleDateFormat.parse(sSelectDate);
        }catch (ParseException e){

        }
        item.setVisible(false);
        getShiftwiseGeneralReportfromServer(sspMilkShift,serverDateFormat.format(selectDate));
    }
    public void getShiftwiseGeneralReportfromServer(String shift,String date){
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ShiftWiseGeneralReportModel> shiftWiseGeneralReportModelCall = apiInterface.getShiftWiseGenReport(mySharedPreference.getUserId(),shift,date);
        shiftWiseGeneralReportModelCall.enqueue(new Callback<ShiftWiseGeneralReportModel>() {
            @Override
            public void onResponse(Call<ShiftWiseGeneralReportModel> call, Response<ShiftWiseGeneralReportModel> response) {
                loadingView.hideDialog();
                shiftWiseGeneralReportModel = response.body();
                if(shiftWiseGeneralReportModel != null){

                    item.setVisible(true);
//                    if (shiftWiseGeneralReportModel.getTotalMilk() != null) {
                        tvTotalMilkltr.setText(shiftWiseGeneralReportModel.getTotalMilk());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }if (shiftWiseGeneralReportModel.getTotalRs() != null) {
                        tvTotalRs.setText(shiftWiseGeneralReportModel.getTotalRs());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }if (shiftWiseGeneralReportModel.getTotalAvgFat() != null) {
                        tvTotalAvFat.setText(shiftWiseGeneralReportModel.getTotalAvgFat());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }if (shiftWiseGeneralReportModel.getBufMilkLtr() != null) {
                        tvBuffaloMilkltr.setText(shiftWiseGeneralReportModel.getBufMilkLtr());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }if (shiftWiseGeneralReportModel.getBufRs() != null) {
                        tvBuffaloTotalRs.setText(shiftWiseGeneralReportModel.getBufRs());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }if (shiftWiseGeneralReportModel.getBufAvgFat() != null) {
                        tvBuffaloAvFat.setText(shiftWiseGeneralReportModel.getBufAvgFat());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }if (shiftWiseGeneralReportModel.getCowMilkLtr() != null) {
                        tvCowMilkltr.setText(shiftWiseGeneralReportModel.getCowMilkLtr());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }if (shiftWiseGeneralReportModel.getCowRs() != null) {
                        tvCowTotalRs.setText(shiftWiseGeneralReportModel.getCowRs());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }if (shiftWiseGeneralReportModel.getCowAvgFat() != null) {
                        tvCowAvFat.setText(shiftWiseGeneralReportModel.getCowAvgFat());
//                    }else {
//                        tvTotalMilkltr.setText("0");
//                    }

                }else {
                    item.setVisible(false);
                    Toast.makeText(getApplicationContext(),"no data found",Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<ShiftWiseGeneralReportModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
