package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.model.UserRegisterModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.etDairyName)
    EditText etDairyName;
    @BindView(R.id.etYourName)
    EditText etYourName;
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.etTaluka)
    EditText etTaluka;
    @BindView(R.id.etDistrict)
    EditText etDistrict;
    @BindView(R.id.etPincode)
    EditText etPincode;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;
    @BindView(R.id.spDairyType)
    Spinner spDairyType;
    @BindView(R.id.cbTermsAndConditions)
    CheckBox cbTermsAndConditions;
    LoadingView loadingView;
    MySharedPreference mySharedPreference;
    String dairyType[] = {"Amul"};
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {

        loadingView = new LoadingView(this);
        mySharedPreference = new MySharedPreference(this);

        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,dairyType);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDairyType.setAdapter(arrayAdapter);
    }

    @OnClick(R.id.tvLogin)
    public void ontvLogin(){
        Intent intent = new Intent(getApplicationContext(),loginActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnRegister)
    public void onbtnRegister(){
        String sDairyName = etDairyName.getText().toString();
        String sYourName = etYourName.getText().toString();
        String sMobile = etMobile.getText().toString();
        String sEmail = etEmail.getText().toString();
        String sCity = etCity.getText().toString();
        String sTaluka = etTaluka.getText().toString();
        String sDistrict = etDistrict.getText().toString();
        String sPincode = etPincode.getText().toString();
        String sPassword = etPassword.getText().toString();
        String sConfirmPassword = etConfirmPassword.getText().toString();
        String sDairyType = spDairyType.getSelectedItem().toString();
        String sTermsAndConditions = cbTermsAndConditions.getText().toString();

        if(sDairyName.isEmpty()){
            Snackbar.make(etDairyName,"enter dairy name",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sYourName.isEmpty()){
            Snackbar.make(etYourName,"enter your name",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sMobile.isEmpty()){
            Snackbar.make(etMobile,"enter mobile number",Snackbar.LENGTH_LONG).show();
            return;
        }else if(!sMobile.matches(mobilePattern)){
            Snackbar.make(etMobile,"enter valid mobile number",Snackbar.LENGTH_LONG).show();
            return;
        } else if(sEmail.isEmpty()){
            Snackbar.make(etEmail,"enter Email id",Snackbar.LENGTH_LONG).show();
            return;
        } else if(!sEmail.matches(emailPattern)){
            Snackbar.make(etEmail,"enter valid Email id",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sCity.isEmpty()){
            Snackbar.make(etCity,"enter City name",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sTaluka.isEmpty()){
            Snackbar.make(etTaluka,"enter Taluka name",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sDistrict.isEmpty()){
            Snackbar.make(etDistrict,"enter District name",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sPincode.isEmpty()){
            Snackbar.make(etPincode,"enter Pincode name",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sPassword.isEmpty()){
            Snackbar.make(etPassword,"enter password",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sConfirmPassword.isEmpty()){
            Snackbar.make(etConfirmPassword,"enter Confirm password",Snackbar.LENGTH_LONG).show();
            return;
        }else if (sPassword.length()<6){
            Snackbar.make(etPassword,"Password should be 6 character",Snackbar.LENGTH_LONG).show();
            return;
        } else if(!sPassword.equals(sConfirmPassword)){
            Snackbar.make(etConfirmPassword,"confirm password not match",Snackbar.LENGTH_LONG).show();
            return;
        }else if(!cbTermsAndConditions.isChecked()){
            Snackbar.make(cbTermsAndConditions,"Please check terms and conditions",Snackbar.LENGTH_LONG).show();
            return;
        }

        sendRegisterDataToserver(sDairyName,sYourName,sMobile,sEmail,sPassword,18,sCity,sTaluka,sDistrict,sPincode);

    }
    private void sendRegisterDataToserver(String dairyName,String dairyOwnerName,String mobileNumber,String email,String password,int dairy_id,String village,String taluka,String district,String pincode) {
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserRegisterModel> userRegisterModelCall  = apiInterface.registerUser(dairyName,dairyOwnerName,mobileNumber,email,password,dairy_id,village,taluka,district,pincode);

        userRegisterModelCall.enqueue(new Callback<UserRegisterModel>() {
            @Override
            public void onResponse(Call<UserRegisterModel> call, Response<UserRegisterModel> response) {
                loadingView.hideDialog();
                UserRegisterModel userRegisterModel = response.body();
                if(userRegisterModel != null){
                    if(userRegisterModel.getResult()){
                        if (userRegisterModel.equals(mobileNumber)){
                            Snackbar.make(etMobile,"Mobile already exists",Snackbar.LENGTH_LONG).show();
                            return;
                        }
                        Toast.makeText(getApplicationContext(),userRegisterModel.getMsg(),Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(RegisterActivity.this,loginActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(getApplicationContext(),userRegisterModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),userRegisterModel.getMsg(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserRegisterModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
