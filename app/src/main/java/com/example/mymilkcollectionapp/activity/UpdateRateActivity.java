package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.myUtils.GlobalMethods;
import com.example.mymilkcollectionapp.myUtils.MilkTypeInterface;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateRateActivity extends AppCompatActivity {

    MySharedPreference mySharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_rate);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.Update_Rate);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnFatWisePrice)
    public void onbtnFatWisePrice(View view){
        showDialogs(1);
    }

    @OnClick(R.id.btnFixPrice)
    public void onbtnFixPrice(View view){
        showDialogs(0);
    }
    @OnClick(R.id.btnFatAndSNFWisePrice)
    public void onbtnFatAndSNFWisePrice(View view){
        showDialogs(2);
    }
    private void showDialogs(int i) {
        GlobalMethods.showDialog(this, new MilkTypeInterface() {
            @Override
            public void onBuffaloMilkTypeInterface() {
                String milk_type = "buffalo";
                if(i == 0){
                    Intent intent = new Intent(getApplicationContext(),FixPriceActivity.class);
                    intent.putExtra("milk_type",milk_type);
                    startActivity(intent);
                }else if(i == 1){
                    Intent intent = new Intent(getApplicationContext(),FatWisePriceActivity.class);
                    intent.putExtra("milk_type",milk_type);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getApplicationContext(),FatAndSnfWisePriceActivity.class);
                    intent.putExtra("milk_type",milk_type);
                    startActivity(intent);
                }
            }

            @Override
            public void onCowMilkTypeInterface() {
                String milk_type = "cow";
                if(i == 0){
                    Intent intent = new Intent(getApplicationContext(),FixPriceActivity.class);
                    intent.putExtra("milk_type",milk_type);
                    startActivity(intent);
                }else if(i == 1){
                    Intent intent = new Intent(getApplicationContext(),FatWisePriceActivity.class);
                    intent.putExtra("milk_type",milk_type);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getApplicationContext(),FatAndSnfWisePriceActivity.class);
                    intent.putExtra("milk_type",milk_type);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancleInterface() {

            }
        });
    }

}
