package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.adapter.CustomerListAdapter;
import com.example.mymilkcollectionapp.adapter.CustomerWiseReportAdapter;
import com.example.mymilkcollectionapp.adapter.PaymentReportAdapter;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.CustomerWiseReportModel;
import com.example.mymilkcollectionapp.model.CustomerWiseReportResponseModel;
import com.example.mymilkcollectionapp.model.PaymentReportModel;
import com.example.mymilkcollectionapp.model.PaymentReportResponseModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.uttampanchasara.pdfgenerator.CreatePdf;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerWiseReportActivity extends AppCompatActivity {

    @BindView(R.id.tvFromDate)
    TextView tvFromDate;
    @BindView(R.id.tvToDate)
    TextView tvToDate;
    @BindView(R.id.rvCustomerWiseReport)
    RecyclerView rvCustomerWiseReport;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    @BindView(R.id.tvTotalRs)
    TextView tvTotalRs;
    @BindView(R.id.llDataNotFound)
    LinearLayout llDataNotFound;
    @BindView(R.id.etCustomerCode)
    EditText etCustomerCode;
    MySharedPreference mySharedPreference;
    Calendar fromDatecalendar;
    Calendar toDateCalendar;
    DatePickerDialog.OnDateSetListener datePickerfromdate;
    DatePickerDialog.OnDateSetListener datePickertodate;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat serverDateFormat;
    LoadingView loadingView;
    Date from_start_Date,to_end_Date;
    List<CustomerWiseReportModel> customerWiseReportModelList;
    CustomerWiseReportAdapter customerWiseReportAdapter;
    DecimalFormat format;
    private MenuItem item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_wise_report);
        ButterKnife.bind(this);
        initAllControls();
    }
    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.customer_wise_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        mySharedPreference = new MySharedPreference(this);
        loadingView = new LoadingView(this);
        format = new DecimalFormat("#.##");

        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);

        serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()).format(new Date());
        tvFromDate.setText(format);
        tvToDate.setText(format);
        fromDatecalendar = Calendar.getInstance();
        datePickerfromdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                fromDatecalendar.set(Calendar.YEAR,year);
                fromDatecalendar.set(Calendar.MONTH,month);
                fromDatecalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateLable();
            }

        };
        toDateCalendar  = Calendar.getInstance();
        datePickertodate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                toDateCalendar.set(Calendar.YEAR,year);
                toDateCalendar.set(Calendar.MONTH,month);
                toDateCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateLable();
            }

        };

        customerWiseReportModelList = new ArrayList<>();

         customerWiseReportAdapter =new CustomerWiseReportAdapter(this, customerWiseReportModelList);

        RecyclerView.LayoutManager layoutManager =new LinearLayoutManager(this);
        rvCustomerWiseReport.setLayoutManager(layoutManager);
        rvCustomerWiseReport.setAdapter(customerWiseReportAdapter);

    }
    @OnClick(R.id.btnGo)
    public void onbtnGo(View view){
        String fromDate = tvFromDate.getText().toString();
        String toDate = tvToDate.getText().toString();
        String sC_code = etCustomerCode.getText().toString();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            from_start_Date = simpleDateFormat.parse(fromDate);
            to_end_Date = simpleDateFormat.parse(toDate);
        }catch (ParseException e){

        }
        item.setVisible(false);
        getPaymentReportDatafromServer(serverDateFormat.format(from_start_Date),serverDateFormat.format(to_end_Date),sC_code);

    }


    @OnClick(R.id.llSelectFromDate)
    public void onivSelectFromDate(View view){
        new DatePickerDialog(CustomerWiseReportActivity.this,datePickerfromdate,fromDatecalendar.get(Calendar.YEAR),fromDatecalendar.get(Calendar.MONTH),fromDatecalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    @OnClick(R.id.llSelectToDate)
    public void onivSelectToDate(View view){
        new DatePickerDialog(CustomerWiseReportActivity.this,datePickertodate, toDateCalendar.get(Calendar.YEAR), toDateCalendar.get(Calendar.MONTH), toDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_download,menu);
        item = menu.findItem(R.id.download_report);
        item.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }if(item.getItemId() == R.id.download_report){
            if (customerWiseReportModelList.size() > 0) {
                Dexter.withActivity(this)
                        .withPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        /* ... */
                        if (report.areAllPermissionsGranted()) {
                            DownloadPdf();
                            Toast.makeText(getApplicationContext(), "Download", Toast.LENGTH_LONG).show();
                        } else {
                            showPermissionDailog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
            } else {
                Toast.makeText(getApplicationContext(), "no data found", Toast.LENGTH_LONG).show();
            }
        }
        return true;
    }
    private File createDirectory() {
        File mFile;
        if (isExternalStorageWritable()) {
            mFile = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "milk_collection");
        } else {
            mFile = new File(Environment.getDataDirectory().toString() + File.separator + "milk_collection");
        }

        if (!mFile.exists())
            mFile.mkdirs();
        return mFile;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private void DownloadPdf() {
        File directory = createDirectory();
        String content = "<html>" +
                "    <body>" +
                "        <table>" +
                "            <tbody>" +
                "                <tr>     " +
                "                    <table width='100%' >" +
                "                        <tbody>" +
                "                            <tr>" +
                "                                <th colspan='3'><h1>" + mySharedPreference.getUserDairyName() + "</h1></th>" +
                "                            </tr>" +
                "                            <tr>" +
                "                                <td>" +
                "                                    " + getString(R.string.from_Date) + " :- " + simpleDateFormat.format(from_start_Date) +
                "                                </td>" +
                "                                <td align='center'>" +
                "                                    " + getString(R.string.to_Date) + " :- " + simpleDateFormat.format(to_end_Date) +
                "                                </td>" +
                "                                <td align='right'>" +
                "                                   " + getString(R.string.date) + " :- " + simpleDateFormat.format(new Date()) +
                "                                </td>" +
                "                            </tr>" +
                "                            <tr>" +
                "                                <td>" +
                "                                    " + getString(R.string.customer_code) + " :- " + customerWiseReportModelList.get(0).getMeCustomerCode() +
                "                                </td>" +
                "                                <td colspan='2' align='right'>" +
                "                                    " + getString(R.string.Customer_Name) + " :- " + customerWiseReportModelList.get(0).getMeCustomerName() +
                "                                </td>" +
                "                            </tr>" +
                "                        </tbody>" +
                "                    </table>" +
                "                    <hr>" +
                "                    <table border='1' cellspacing='0' align='center' width='100%'>" +
                "                        <tbody>" +
                "                            <tr>" +
                "                                <th colspan='8'><font size='5'>" + getResources().getString(R.string.customer_wise_report) + "</font></th>" +
                "                            </tr>" +
                "                            <tr>" +
                "" +
                "                                <th>" + getString(R.string.date) + "</th>" +
                "                                <th>" + getString(R.string.milk_type) + "</th>" +
                "                                <th>" + getString(R.string.Weight) + "</th>" +
                "                                <th>" + getString(R.string.FAT) + "</th>" +
                "                                <th>" + getString(R.string.Fat_rat) + "</th>" +
                "                                <th>" + getString(R.string.shift) + "</th>" +
                "                                <th>" + getString(R.string.Rate_per_ltr) + "</th>" +
                "                                <th>" + getString(R.string.total_rs) + "</th>" +
                "                            </tr>";
        for (CustomerWiseReportModel addMilk : customerWiseReportModelList) {

            content += "                <tr>";
            try {
                content += "                    <td align='center'>" + simpleDateFormat.format(serverDateFormat.parse(addMilk.getMeDate())) + "</td>";
            } catch (ParseException e) {
                e.printStackTrace();
            }

            content += "                    <td align='center'>" + (addMilk.getMeMilkType().equals("buffalo") ? getString(R.string.buffalo) : getString(R.string.cow)) + "</td>" +
                    "                    <td align='center'>" + addMilk.getMeWeight() + " " + addMilk.getMeWeightType() + "</td>" +
                    "                    <td align='center'>" + addMilk.getMeFat() + "</td>" +
                    "                    <td align='center'>" + addMilk.getMeFatPrice() + "</td>" +
                    "                    <td align='center'>" + (addMilk.getMeShift().equals("Morning") ? getString(R.string.morning) : getString(R.string.evening)) + "</td>" +
                    "                    <td align='center'>" + format.format(Double.parseDouble(addMilk.getMePerUnitPrice())) + "</td>" +
                    "                    <td align='center'>" + addMilk.getMeTotalPrice() + "</td>" +
                    "                </tr>";

        }
        content += "            </tbody>" +
                "                    </table>" +
                "                </tr>" +
                "            </tbody>" +
                "        </table>       " +
                "    </body>" +
                "</html>";

        new CreatePdf(getApplicationContext())
                .setPdfName("Customer_report_" + simpleDateFormat.format(from_start_Date) + "_" + simpleDateFormat.format(to_end_Date) + "_" + customerWiseReportModelList.get(0).getMeCustomerCode())
                .openPrintDialog(false)
                .setContentBaseUrl(null)
                .setContent(content)
                .setFilePath(directory.getAbsolutePath() + File.separator)
                .setCallbackListener(new CreatePdf.PdfCallbackListener() {
                    @Override
                    public void onFailure(String s) {
                        Toast.makeText(getApplicationContext(), "download failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(String s) {
                        String file_path=directory.getAbsolutePath() + File.separator+"Customer_report_" + simpleDateFormat.format(from_start_Date) + "_" + simpleDateFormat.format(to_end_Date) + "_" + customerWiseReportModelList.get(0).getMeCustomerCode();
                        Toast.makeText(getApplicationContext(), " download complete"+file_path, Toast.LENGTH_SHORT).show();
                    }
                })
                .create();

    }

    public void showPermissionDailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CustomerWiseReportActivity.this);
        builder.setTitle("Need Send SMS Permission");
        builder.setMessage("This app needs SMS permission for send please give permission from settings.");
        builder.setPositiveButton("Go To Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void updateLable() {
        String format = "dd-MM-yyyy";
        simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        tvFromDate.setText(simpleDateFormat.format(fromDatecalendar.getTime()));
        tvToDate.setText(simpleDateFormat.format(toDateCalendar.getTime()));

    }

    public void getPaymentReportDatafromServer(String start_date ,String end_date,String c_code){
        loadingView.showDialog();
        customerWiseReportModelList.clear();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CustomerWiseReportResponseModel> customerWiseReportResponseModelCall = apiInterface.getCustomerWiseReport(start_date,end_date,mySharedPreference.getUserId(),c_code);
        customerWiseReportResponseModelCall.enqueue(new Callback<CustomerWiseReportResponseModel>() {
            @Override
            public void onResponse(Call<CustomerWiseReportResponseModel> call, Response<CustomerWiseReportResponseModel> response) {
                loadingView.hideDialog();
                CustomerWiseReportResponseModel customerWiseReportResponseModel = response.body();
                if(customerWiseReportResponseModel != null){
                    item.setVisible(true);
                    if(customerWiseReportResponseModel.getResult()){

                        customerWiseReportModelList.addAll(customerWiseReportResponseModel.getData());
                        customerWiseReportAdapter.notifyDataSetChanged();
                        llDataNotFound.setVisibility(View.GONE);
                        rvCustomerWiseReport.setVisibility(View.VISIBLE);

                        int total_weight = 0;
                        int total_Amount = 0;
                        for (CustomerWiseReportModel customerWiseReportModel : customerWiseReportModelList) {
                            total_weight += Double.parseDouble(customerWiseReportModel.getMeWeight());
                            total_Amount += Double.parseDouble(customerWiseReportModel.getMeTotalPrice());
                        }
                        tvTotal.setText("" + total_weight);
                        tvTotalRs.setText("" + total_Amount);
                        if (customerWiseReportModelList.size()<1){
                            llDataNotFound.setVisibility(View.VISIBLE);
                            rvCustomerWiseReport.setVisibility(View.INVISIBLE);
                            item.setVisible(false);
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Data not Found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    item.setVisible(false);
                    Toast.makeText(getApplicationContext(),"Data not Found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CustomerWiseReportResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
