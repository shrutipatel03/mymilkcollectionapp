package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.adapter.CustomerListAdapter;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.GetCustomerListModel;
import com.example.mymilkcollectionapp.model.GetCustomerListResponseModel;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomersActivity extends AppCompatActivity {

    CustomerListAdapter customerListAdapter;
    List<GetCustomerListModel> getCustomerListModelList;
    @BindView(R.id.rvCoustomerList)
    RecyclerView rvCustomerList;
    @BindView(R.id.etSearchCustomer)
    EditText etSearchCustomer;
    MySharedPreference mySharedPreference;
    LoadingView loadingView;
    GetCustomerListModel getCustomerListModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.customer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        getCustomerListModelList = new ArrayList<>();
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
        loadingView = new LoadingView(this);
        customerListAdapter = new CustomerListAdapter(this, getCustomerListModelList, new CustomerListAdapter.OnItemClickListener() {
            @Override
            public void onItemUpdateListener(int position) {
                getCustomerListModel = getCustomerListModelList.get(position);
                Intent intent = new Intent(getApplicationContext(),NewCustomerActivity.class);
                intent.putExtra("data",getCustomerListModel);
                startActivityForResult(intent,101);
            }

            @Override
            public void onItemDeleteListener(int position) {
                getCustomerListModel = getCustomerListModelList.get(position);
                deleteCustomerDatafromServer();
            }
        });
        etSearchCustomer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                customerListAdapter.getFilter().filter(s.toString());
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvCustomerList.setLayoutManager(layoutManager);
        rvCustomerList.setAdapter(customerListAdapter);
        getCustomerDatafromServer();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {

            getCustomerDatafromServer();

        }
    }

    @OnClick(R.id.btnNewCustomer)
    public void onbtnNewCustomer(View view){
        Intent intent = new Intent(getApplicationContext(),NewCustomerActivity.class);
        startActivityForResult(intent,101);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getCustomerDatafromServer(){
        loadingView.showDialog();
        getCustomerListModelList.clear();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<GetCustomerListResponseModel> getCustomerListResponseModelCall = apiInterface.getCustomerList(mySharedPreference.getUserId());
        getCustomerListResponseModelCall.enqueue(new Callback<GetCustomerListResponseModel>() {
            @Override
            public void onResponse(Call<GetCustomerListResponseModel> call, Response<GetCustomerListResponseModel> response) {
                loadingView.hideDialog();
                GetCustomerListResponseModel getCustomerListResponseModel = response.body();
                if(getCustomerListResponseModel != null){
                    if(getCustomerListResponseModel.getResult()){
                        getCustomerListModelList.addAll(getCustomerListResponseModel.getData());
                        customerListAdapter.notifyDataSetChanged();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"data not found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"data not found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GetCustomerListResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
    public void deleteCustomerDatafromServer(){
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> deleteCustomerDataCall = apiInterface.deleteCustomerData(getCustomerListModel.getCustomerId());
        deleteCustomerDataCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel getCustomerListResponseModel = response.body();
                if(getCustomerListResponseModel != null){
                    if(getCustomerListResponseModel.getResult()){
                        getCustomerDatafromServer();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"something wrong",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"something wrong",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
