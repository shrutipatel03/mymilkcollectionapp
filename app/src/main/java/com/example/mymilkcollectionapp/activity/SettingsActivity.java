package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity {

    MySharedPreference mySharedPreference;
    @BindView(R.id.rgLanguage)
    RadioGroup rgLanguage;
    @BindView(R.id.rbEnglish)
    RadioButton rbEnglish;
    @BindView(R.id.rbGujarati)
    RadioButton rbGujarati;
    @BindView(R.id.rbHindi)
    RadioButton rbHindi;
    @BindView(R.id.rbFixPrice)
    RadioButton rbFixPrice;
    @BindView(R.id.rbFatWisePrice)
    RadioButton rbFatWisePrice;
    @BindView(R.id.rbFatandSNFWisePrice)
    RadioButton rbFatandSNFWisePrice;
    @BindView(R.id.rgprice)
    RadioGroup rgprice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.action_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));

        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);

        if(mySharedPreference.getLanguage().equals("en")){
            rbEnglish.setChecked(true);
        }else if(mySharedPreference.getLanguage().equals("gu")){
            rbGujarati.setChecked(true);
        }else if(mySharedPreference.getLanguage().equals("hi")){
            rbHindi.setChecked(true);
        }

        if(mySharedPreference.getPriceMethod().equals("fix")){
            rbFixPrice.setChecked(true);
        }else if(mySharedPreference.getPriceMethod().equals("fat")){
            rbFatWisePrice.setChecked(true);
        }else if(mySharedPreference.getPriceMethod().equals("fat and snf")){
            rbFatandSNFWisePrice.setChecked(true);
        }

        rgLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (rbEnglish.isChecked()){
                    mySharedPreference.setLanguage("en");
                    restartActivity();
                }else if (rbGujarati.isChecked()){
                    mySharedPreference.setLanguage("gu");
                    restartActivity();
                }else if (rbHindi.isChecked()){
                    mySharedPreference.setLanguage("hi");
                    restartActivity();
                }
            }
        });
        rgprice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (rbFixPrice.isChecked()){
                    mySharedPreference.setPriceMethod("fix");
                    restartActivity();
                }else if (rbFatWisePrice.isChecked()){
                    mySharedPreference.setPriceMethod("fat");
                    restartActivity();
                }else if (rbFatandSNFWisePrice.isChecked()){
                    mySharedPreference.setPriceMethod("fat and snf");
                    restartActivity();
                }
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    public void restartActivity(){
        Intent intent = new Intent(getApplicationContext(),loginActivity.class);
        finish();
        startActivity(intent);
    }
}
