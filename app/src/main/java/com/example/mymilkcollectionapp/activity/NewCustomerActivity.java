package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.GetCustomerListModel;
import com.example.mymilkcollectionapp.model.LoginModel;
import com.example.mymilkcollectionapp.model.LoginResponseModel;
import com.example.mymilkcollectionapp.model.NewCustomerResponseModel;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.google.android.material.snackbar.Snackbar;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCustomerActivity extends AppCompatActivity {

    @BindView(R.id.etCustomerCode)
    EditText etCustomerCode;
    @BindView(R.id.etCustomerName)
    EditText etCustomerName;
    @BindView(R.id.etCustomerMobile)
    EditText etCustomerMobile;
    @BindView(R.id.btnSaveCustomer)
    Button btnSaveCustomer;
//    @BindView(R.id.rgPaymentMode)
//    RadioGroup rgPaymentMode;
//    @BindView(R.id.rbBank)
//    RadioButton rbBank;
//    @BindView(R.id.rbCash)
//    RadioButton rbCash;
//    private String payment_mode = "cash";
    private String TAG = "NewCustomerActivity";
    MySharedPreference mySharedPreference;
    LoadingView loadingView;
    GetCustomerListModel getCustomerListModel;
    String mobilePattern = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.Add_customer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);

        loadingView = new LoadingView(this);
        Intent intent = getIntent();
        getCustomerListModel = intent.getParcelableExtra("data");
        if(intent.hasExtra("data")){
            btnSaveCustomer.setText("update");
            getSupportActionBar().setTitle("Update Customers");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));

            etCustomerCode.setText(getCustomerListModel.getCustomerCode());
            etCustomerName.setText(getCustomerListModel.getCustomerName());
            etCustomerMobile.setText(getCustomerListModel.getCustomerMobileNumber());
        }
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnSaveCustomer)
    public void onbtnNewCustomer(View view){
        String sCustomerCode = etCustomerCode.getText().toString();
        String sCustomerName = etCustomerName.getText().toString();
        String sCustomerMobile = etCustomerMobile.getText().toString();

        if(sCustomerCode.isEmpty()){
            Snackbar.make(etCustomerCode,"enter customer code",Snackbar.LENGTH_LONG).show();
            return;
        }
        else if(sCustomerName.isEmpty()){
            Snackbar.make(etCustomerName,"enter customer Name",Snackbar.LENGTH_LONG).show();
            return;
        }
        else if(sCustomerMobile.isEmpty()){
            Snackbar.make(etCustomerMobile,"enter customer Mobile",Snackbar.LENGTH_LONG).show();
            return;
        }else if (!sCustomerMobile.matches(mobilePattern)){
            Snackbar.make(etCustomerMobile,"enter valid Mobile number",Snackbar.LENGTH_LONG).show();
            return;
        }

        if(btnSaveCustomer.getText().toString().equals("update")){
            sendUpdateDataToServer(sCustomerCode,sCustomerName,sCustomerMobile);
        }
        else {
            sendDataToServer(sCustomerCode,sCustomerName,sCustomerMobile);
        }
    }

    private void sendDataToServer(String sCustomerCode,String sCustomerName,String sCustomerMobile) {
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewCustomerResponseModel> newCustomerResponseModelCall = apiInterface.getCustomerData(sCustomerCode,sCustomerName,sCustomerMobile,mySharedPreference.getUserId());
        Log.d(TAG,"id" +mySharedPreference.getUserId());
        newCustomerResponseModelCall.enqueue(new Callback<NewCustomerResponseModel>() {
            @Override
            public void onResponse(Call<NewCustomerResponseModel> call, Response<NewCustomerResponseModel> response) {
                loadingView.hideDialog();
                NewCustomerResponseModel newCustomerResponseModel = response.body();
                if(newCustomerResponseModel != null){
                    if(newCustomerResponseModel.getResult()){
                        Toast.makeText(getApplicationContext(),newCustomerResponseModel.getMsg(),Toast.LENGTH_LONG).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),newCustomerResponseModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),newCustomerResponseModel.getMsg(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<NewCustomerResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
    private void sendUpdateDataToServer(String sCustomerCode,String sCustomerName,String sCustomerMobile) {
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> updateCustomerModelCall = apiInterface.updateCustomerData(sCustomerCode,sCustomerName,sCustomerMobile,mySharedPreference.getUserId(),getCustomerListModel.getCustomerId());
        Log.d(TAG,"id" +mySharedPreference.getUserId());
        updateCustomerModelCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel updateCustomerModel = response.body();
                if(updateCustomerModel != null){
                    if(updateCustomerModel.getResult()){
                        Toast.makeText(getApplicationContext(),updateCustomerModel.getMsg(),Toast.LENGTH_LONG).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),updateCustomerModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),updateCustomerModel.getMsg(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}

