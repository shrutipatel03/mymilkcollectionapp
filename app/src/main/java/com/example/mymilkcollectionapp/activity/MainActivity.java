package com.example.mymilkcollectionapp.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.TextView;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    MySharedPreference mySharedPreference;
    public static MainActivity mainActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainActivity = this;
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        MenuItem nav_logout = menu.findItem(R.id.nav_logout);
        View header = navigationView.getHeaderView(0);
        TextView userName = header.findViewById(R.id.tvUserName);
        TextView userEmail = header.findViewById(R.id.tvUserEmail);
        TextView userIcon = header.findViewById(R.id.tvUserIcon);
        userName.setText(mySharedPreference.getUserName());
        userEmail.setText(mySharedPreference.getEmail());
        if(mySharedPreference.getUserName().trim().length()>0){
        userIcon.setText(mySharedPreference.getUserName().toUpperCase().substring(0,1));}
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        nav_logout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mySharedPreference.is_userLogout();
                Intent intent = new Intent(MainActivity.this,loginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;

            }
        });
    }

   public static MainActivity getMainActivity(){
        return mainActivity;
   }

   public void setNavigationHeader(){
       NavigationView navigationView = findViewById(R.id.nav_view);
       View header = navigationView.getHeaderView(0);
       TextView userName = header.findViewById(R.id.tvUserName);
       TextView userEmail = header.findViewById(R.id.tvUserEmail);
       TextView userIcon = header.findViewById(R.id.tvUserIcon);
       userName.setText(mySharedPreference.getUserName());
       userEmail.setText(mySharedPreference.getEmail());
       if(mySharedPreference.getUserName().trim().length()>0){
           userIcon.setText(mySharedPreference.getUserName().toUpperCase().substring(0,1));}
   }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
