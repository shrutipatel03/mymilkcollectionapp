package com.example.mymilkcollectionapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.roomDatabaseTable.FatandSNFPriceTable;
import com.example.mymilkcollectionapp.roomDatabaseTable.FixPriceTable;
import com.example.mymilkcollectionapp.database.MyRoomDatabase;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.GetCustomerListModel;
import com.example.mymilkcollectionapp.model.GetCustomerListResponseModel;
import com.example.mymilkcollectionapp.model.GetUserWiseMilkEntryModel;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.example.mymilkcollectionapp.roomDatabaseTable.GetUserWisePriceTable;
import com.google.android.material.snackbar.Snackbar;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMilkActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.etAddMilkCustomerCode)
    EditText etAddMilkCustomerCode;
    @BindView(R.id.tvAddMilkCustomerName)
    TextView tvAddMilkCustomerName;
    @BindView(R.id.etAddMilkFAT)
    EditText etAddMilkFAT;
    @BindView(R.id.etAddMilkWeight)
    EditText etAddMilkWeight;
    @BindView(R.id.etAddMilkTotal)
    EditText etAddMilkTotal;
    @BindView(R.id.rbCow)
    RadioButton rbCow;
    @BindView(R.id.rbBuffalo)
    RadioButton rbBuffalo;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.spMilkShift)
    Spinner spMilkShift;
    @BindView(R.id.etAddMilkSNF)
    EditText etAddMilkSNF;
    @BindView(R.id.etAddMilkCLR)
    EditText etAddMilkCLR;
    @BindView(R.id.rgMilkType)
    RadioGroup rgMilkType;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.tvFatValidation)
    TextView tvFatValidation;
    List<GetCustomerListModel> getCustomerListModelList;
    List<FixPriceTable> fixPriceTableList;
    List<FatandSNFPriceTable> fatandSNFPriceTableList;
    GetCustomerListModel getCustomerListModel;
    LoadingView loadingView;
    MySharedPreference mySharedPreference;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener date;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat serverDateFormat;
    Date selectedDate;
    String fs_id;
    String TAG = "AddMilkActivity";
    double fat_price;
    double per_unit_price;
    String selectedMilkType;
    private String milkType;
    DecimalFormat format;
    GetUserWiseMilkEntryModel getUserWiseMilkEntryModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_milk);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setTitle(R.string.Add_Milk);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));

        getCustomerListModelList = new ArrayList<>();
        fixPriceTableList = new ArrayList<>();
        fatandSNFPriceTableList = new ArrayList<>();
        getCustomerListModel = new GetCustomerListModel();
        loadingView = new LoadingView(this);
        mySharedPreference = new MySharedPreference(this);
        Locale locales = new Locale(mySharedPreference.getLanguage());
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locales;
        resources.updateConfiguration(configuration,displayMetrics);
        String milkShift[] = {getResources().getString(R.string.morning),getResources().getString(R.string.evening)};
        format = new DecimalFormat("#.##");
        serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        spMilkShift.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,milkShift);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMilkShift.setAdapter(arrayAdapter);
        String format = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()).format(new Date());
        tvDate.setText(format);
        calendar = Calendar.getInstance();

        rgMilkType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {


            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(!etAddMilkFAT.equals("")){
                    if(rbBuffalo.isChecked()){
                        String fat = etAddMilkFAT.getText().toString();
                        String weight = etAddMilkWeight.getText().toString();
                        String snf = etAddMilkSNF.getText().toString();
                        if(fat.trim().length() != 0 && weight.trim().length() != 0){
                            if(mySharedPreference.getPriceMethod().equals("fix")){
                                getFixPrice(fat,weight,milkType);
                            }else if(mySharedPreference.getPriceMethod().equals("fat and snf")){
                                getFatandSNFWisePriceData(snf,fat,weight,milkType);
                            }else if (mySharedPreference.getPriceMethod().equals("fat")){
                                getUserWisePriceData(fat,weight,milkType);
                            }
                        }else{
                            etAddMilkTotal.setText(String.valueOf(0));
                        }
                    }
                    else if(rbCow.isChecked()){
                        String fat = etAddMilkFAT.getText().toString();
                        String weight = etAddMilkWeight.getText().toString();
                        String snf = etAddMilkSNF.getText().toString();
                        if(fat.trim().length() != 0 && weight.trim().length() != 0){
                            if(mySharedPreference.getPriceMethod().equals("fix")){
                                getFixPrice(fat,weight,milkType);
                            }else if(mySharedPreference.getPriceMethod().equals("fat and snf")){
                                getFatandSNFWisePriceData(snf,fat,weight,milkType);
                            }else if (mySharedPreference.getPriceMethod().equals("fat")){
                                getUserWisePriceData(fat,weight,milkType);
                            }
                        }else{
                            etAddMilkTotal.setText(String.valueOf(0));
                        }
                    }
                }
            }



        });
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateLable();
            }
        };

        if(rbBuffalo.isChecked()){
            fat_price = 5.6;
        }else {
            fat_price = 3.8;
        }

        getCustomerDatafromServer();
        etAddMilkCustomerCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() > 0){

                    if(getCustomerListModelList.size() >0){
                        boolean is_found =false;
                        Log.d("AddMilkActivity","size"+getCustomerListModelList.size());
                        for (GetCustomerListModel temp: getCustomerListModelList){
                            if(temp.getCustomerCode().equals(s.toString())){
                                is_found = true;
                                getCustomerListModel = temp;
                            }

                        }
                        if(is_found) {
                            tvAddMilkCustomerName.setText(getCustomerListModel.getCustomerName());

                        }
                        else {
                            getCustomerListModel = null;
                            tvAddMilkCustomerName.setText("Data Not found");

                        }

                    }

                }
            }
        });

        etAddMilkWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!etAddMilkFAT.equals("")){
                    if(rbBuffalo.isChecked()){
                        String fat = etAddMilkFAT.getText().toString();
                        String weight = etAddMilkWeight.getText().toString();
                        String snf = etAddMilkSNF.getText().toString();
                        if(fat.trim().length() != 0 && weight.trim().length() != 0){
                            if(mySharedPreference.getPriceMethod().equals("fix")){
                                getFixPrice(fat,weight,milkType);
                            }else if(mySharedPreference.getPriceMethod().equals("fat and snf")){
                                getFatandSNFWisePriceData(snf,fat,weight,milkType);
                            }else if (mySharedPreference.getPriceMethod().equals("fat")){
                                fatandSNFPriceTableList.clear();
                                getUserWisePriceData(fat,weight,milkType);
                            }

                        }else{
                            etAddMilkTotal.setText(String.valueOf(0));
                        }
                    }
                    else if(rbCow.isChecked()){
                        String fat = etAddMilkFAT.getText().toString();
                        String weight = etAddMilkWeight.getText().toString();
                        String snf = etAddMilkSNF.getText().toString();
                        if(fat.trim().length() != 0 && weight.trim().length() != 0){
                            if(mySharedPreference.getPriceMethod().equals("fix")){
                                getFixPrice(fat,weight,milkType);
                            }else if(mySharedPreference.getPriceMethod().equals("fat and snf")){
                                getFatandSNFWisePriceData(snf,fat,weight,milkType);
                            }else if (mySharedPreference.getPriceMethod().equals("fat")){
                                fatandSNFPriceTableList.clear();
                                getUserWisePriceData(fat,weight,milkType);
                            }
                        }else{
                            etAddMilkTotal.setText(String.valueOf(0));
                        }
                    }
                }
            }
        });
        etAddMilkFAT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!etAddMilkSNF.equals("") && !etAddMilkFAT.equals("") && etAddMilkCLR.equals("0")){
                    clrCalculation(etAddMilkSNF.getText().toString());
                }if(!etAddMilkCLR.equals("") && !etAddMilkFAT.equals("") && etAddMilkSNF.equals("0")){
                    snfCalculation(etAddMilkCLR.getText().toString());
                }
                if(!etAddMilkWeight.equals("")){
                    if(rbBuffalo.isChecked()){
                        String fat = etAddMilkFAT.getText().toString();
                        String weight = etAddMilkWeight.getText().toString();
                        String snf = etAddMilkSNF.getText().toString();
                        if(fat.trim().length() != 0 && weight.trim().length() != 0){
                            if(mySharedPreference.getPriceMethod().equals("fix")){
                                getFixPrice(fat,weight,milkType);

                            }else if(mySharedPreference.getPriceMethod().equals("fat and snf")){
                                getFatandSNFWisePriceData(snf,fat,weight,milkType);
                            }else if (mySharedPreference.getPriceMethod().equals("fat")){
                                getUserWisePriceData(fat,weight,milkType);
                            }
                        }else{
                            etAddMilkTotal.setText(String.valueOf(0));
                        }
                    }
                    else if(rbCow.isChecked()){
                        String fat = etAddMilkFAT.getText().toString();
                        String weight = etAddMilkWeight.getText().toString();
                        String snf = etAddMilkSNF.getText().toString();
                        if(fat.trim().length() != 0 && weight.trim().length() != 0){
                            if(mySharedPreference.getPriceMethod().equals("fix")){
                                getFixPrice(fat,weight,milkType);
                            }else if (mySharedPreference.getPriceMethod().equals("fat")){
                                getUserWisePriceData(fat,weight,milkType);
                            }else if (mySharedPreference.getPriceMethod().equals("fat and snf")){
                                getFatandSNFWisePriceData(snf,fat,weight,milkType);
                            }
                        }else{
                            etAddMilkTotal.setText(String.valueOf(0));
                        }
                    }
                }

            }
        });
        etAddMilkSNF.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!etAddMilkSNF.equals("")){
                    if(rbBuffalo.isChecked()){
                        String fat = etAddMilkFAT.getText().toString();
                        String weight = etAddMilkWeight.getText().toString();
                        String snf = etAddMilkSNF.getText().toString();
                        if(fat.trim().length() != 0 && weight.trim().length() != 0 && snf.trim().length() != 0){
                           if(mySharedPreference.getPriceMethod().equals("fat and snf")){
                                getFatandSNFWisePriceData(snf,fat,weight,milkType);
                            }
                        }else{
                            etAddMilkTotal.setText(String.valueOf(0));
                        }
                    }else if(rbCow.isChecked()){
                        String fat = etAddMilkFAT.getText().toString();
                        String weight = etAddMilkWeight.getText().toString();
                        String snf = etAddMilkSNF.getText().toString();
                        if(fat.trim().length() != 0 && weight.trim().length() != 0 && snf.trim().length() != 0){
                            if(mySharedPreference.getPriceMethod().equals("fat and snf")){
                                getFatandSNFWisePriceData(snf,fat,weight,milkType);
                            }
                        }else{
                            etAddMilkTotal.setText(String.valueOf(0));
                        }
                    }
                }
                if (!etAddMilkSNF.hasFocus()){
                    return;
                }else {
                    if (s.toString().trim().length()>0){
                        clrCalculation(s.toString());
                    }
                }
            }
        });
        etAddMilkCLR.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                    if (!etAddMilkCLR.hasFocus()){
                        return;
                    }else {
                        if (s.toString().trim().length() > 0) {
                            snfCalculation(s.toString());
                        }
                    }
            }
        });
        Intent intent = getIntent();
        getUserWiseMilkEntryModel = intent.getParcelableExtra("milk_data");

        if ((intent.hasExtra("milk_data"))){
            btnSave.setText("upadte");
            getSupportActionBar().setTitle("Upadte Milk");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_drawable_file));

            etAddMilkCustomerCode.setText(getUserWiseMilkEntryModel.getMeCustomerCode());
            tvAddMilkCustomerName.setText(getUserWiseMilkEntryModel.getMeCustomerName());
            try {
                selectedDate=serverDateFormat.parse(getUserWiseMilkEntryModel.getMeDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tvDate.setText(simpleDateFormat.format(selectedDate));
            etAddMilkWeight.setText(getUserWiseMilkEntryModel.getMeWeight());
            etAddMilkFAT.setText(getUserWiseMilkEntryModel.getMeFat());
            etAddMilkCLR.setText(getUserWiseMilkEntryModel.getMeClr());
            etAddMilkSNF.setText(getUserWiseMilkEntryModel.getMeSnf());
            etAddMilkTotal.setText(getUserWiseMilkEntryModel.getMeTotalPrice());
            if (getUserWiseMilkEntryModel.getMeShift().equals("Morning")){
                spMilkShift.setSelection(0);
            }else {
                spMilkShift.setSelection(1);
            }
            if(getUserWiseMilkEntryModel.getMeMilkType().equalsIgnoreCase("cow")){
                selectedMilkType = "cow";
                rbCow.setChecked(true);
            }else {
                selectedMilkType = "buffalo";
                rbBuffalo.setChecked(true);
            }
        }
    }

    public void snfCalculation(String s){
        String fat = etAddMilkFAT.getText().toString();
        if(fat.trim().length() == 0){
            etAddMilkSNF.setText(String.valueOf(0));
        }else {
            double snf = (Double.parseDouble(s)) - (0.2 * Double.parseDouble(fat)) - ((0.50))*4;
            etAddMilkSNF.setText(""+format.format(snf));
        }
    }
    public void clrCalculation(String s){
        String fat = etAddMilkFAT.getText().toString();
        if(fat.trim().length() == 0 || s.equals(".")){
            etAddMilkCLR.setText(String.valueOf(0));
        }else {
            double clr = (Double.parseDouble(s)) / 4 + 0.2 * Double.parseDouble(fat) + 0.50;
            etAddMilkCLR.setText(""+format.format(clr));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_customer,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.tvDate)
    public void onselectDate(){
        new DatePickerDialog(AddMilkActivity.this,date,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        if(item.getItemId() == R.id.add_customer){
            Intent intent = new Intent(getApplicationContext(),CustomersActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateLable(){
        String format = "dd-MM-yyyy";
        simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        tvDate.setText(simpleDateFormat.format(calendar.getTime()));
    }
    public void getCustomerDatafromServer(){
        loadingView.showDialog();
        getCustomerListModelList.clear();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<GetCustomerListResponseModel> getCustomerListResponseModelCall = apiInterface.getCustomerList(mySharedPreference.getUserId());
        getCustomerListResponseModelCall.enqueue(new Callback<GetCustomerListResponseModel>() {
            @Override
            public void onResponse(Call<GetCustomerListResponseModel> call, Response<GetCustomerListResponseModel> response) {
                loadingView.hideDialog();
                GetCustomerListResponseModel getCustomerListResponseModel = response.body();
                if(getCustomerListResponseModel != null){
                    if(getCustomerListResponseModel.getResult()){
                        getCustomerListModelList.addAll(getCustomerListResponseModel.getData());

                    }
                    else {
                        Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GetCustomerListResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }

    @OnClick(R.id.btnSave)
    public void onbtnSave(View view){
        String sAddMilkCustomerCode = etAddMilkCustomerCode.getText().toString();
        String sAddMilkCustomerName = tvAddMilkCustomerName.getText().toString();
        String sDate = tvDate.getText().toString();
        String sAddMilkFAT = etAddMilkFAT.getText().toString();
        String sAddMilkWeight = etAddMilkWeight.getText().toString();
        String sMilkShift = spMilkShift.getSelectedItem().toString();
        String sAddMilkSNF = etAddMilkSNF.getText().toString();
        String sAddMilkCLR = etAddMilkCLR.getText().toString();
        String sAddMilkTotal = etAddMilkTotal.getText().toString();

        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            selectedDate = simpleDateFormat.parse(sDate);
        }catch (ParseException e){

        }

        if(rbCow.isChecked()){
                selectedMilkType = "cow";
        }else {
                selectedMilkType = "buffalo";
        }

        if(sAddMilkCustomerCode.isEmpty()){
            tvFatValidation.setText("please enter customer code");
            tvFatValidation.setVisibility(View.VISIBLE);
        }else if (getCustomerListModel == null){
            tvFatValidation.setText("please enter right customer code");
            tvFatValidation.setVisibility(View.VISIBLE);
        }else if(sAddMilkWeight.isEmpty()){
            tvFatValidation.setText("please enter weight");
            tvFatValidation.setVisibility(View.VISIBLE);
        }else if(sAddMilkCLR.isEmpty()) {
            tvFatValidation.setText("please enter CLR");
            tvFatValidation.setVisibility(View.VISIBLE);
        }else if(sAddMilkSNF.isEmpty() || sAddMilkSNF.equals(".") || Double.parseDouble(sAddMilkSNF)>9.4 || Double.parseDouble(sAddMilkSNF)<7.6){
            tvFatValidation.setText("snf value must between 7.6 and 9.4");
            tvFatValidation.setVisibility(View.VISIBLE);
        } else if(selectedMilkType.equalsIgnoreCase("cow") && (sAddMilkFAT.isEmpty() || Double.parseDouble(sAddMilkFAT)>5|| Double.parseDouble(sAddMilkFAT)<2.5)){
            tvFatValidation.setText("cow fat value must between 2.5 and 5");
            tvFatValidation.setVisibility(View.VISIBLE);
        }else if (selectedMilkType.equalsIgnoreCase("buffalo") && (sAddMilkFAT.isEmpty() || Double.parseDouble(sAddMilkFAT)>14 || Double.parseDouble(sAddMilkFAT)<5.1)){
            tvFatValidation.setText("Buffalo fat value must between 5.1 and 14");
            tvFatValidation.setVisibility(View.VISIBLE);
        }else {
            if (getIntent().hasExtra("milk_data")) {
                per_unit_price = Double.parseDouble(sAddMilkTotal) / Double.parseDouble(sAddMilkWeight);
                tvFatValidation.setVisibility(View.INVISIBLE);
                sendUpdateDataToServer(sAddMilkCustomerCode, sAddMilkCustomerName, selectedMilkType, serverDateFormat.format(selectedDate), sMilkShift, sAddMilkWeight, sAddMilkFAT, String.valueOf(fat_price), "ltr", sAddMilkSNF, sAddMilkCLR, sAddMilkTotal, String.valueOf(per_unit_price), mySharedPreference.getPriceMethod());

            } else {
                per_unit_price = Double.parseDouble(sAddMilkTotal) / Double.parseDouble(sAddMilkWeight);
                tvFatValidation.setVisibility(View.INVISIBLE);
                sendDataToServer(sAddMilkCustomerCode, sAddMilkCustomerName, selectedMilkType, serverDateFormat.format(selectedDate), sMilkShift, sAddMilkWeight, sAddMilkFAT, String.valueOf(fat_price), "ltr", sAddMilkSNF, sAddMilkCLR, sAddMilkTotal, String.valueOf(per_unit_price), mySharedPreference.getPriceMethod());

                Log.d(TAG, "date " + serverDateFormat.format(selectedDate));
            }
        }

    }

    private void sendDataToServer(String customer_code,String customer_name,String milk_type,String selected_date,String shift,String weight,String fat,String fat_price,String weight_type,String snf,String clr,String total_price,String per_unit_price,String price_calculated_by) {
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        Log.d(TAG,"c_id"+getUserWiseMilkEntryModel.getMeCustomerId());
        Call<StatusModel> addMilkDataCall  = apiInterface.addMilkData(mySharedPreference.getUserId(),getCustomerListModel.getCustomerId(),customer_code,customer_name,milk_type,selected_date,shift,weight,fat,fat_price,weight_type,snf,clr,total_price,per_unit_price,price_calculated_by);
        Log.d(TAG,"id" +mySharedPreference.getUserId());

        addMilkDataCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel addMilkDataCall = response.body();
                if(addMilkDataCall != null){
                    if(addMilkDataCall.getResult()){
                        Toast.makeText(getApplicationContext(),addMilkDataCall.getMsg(),Toast.LENGTH_LONG).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),addMilkDataCall.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),addMilkDataCall.getMsg(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
    private void sendUpdateDataToServer(String customer_code,String customer_name,String milk_type,String selected_date,String shift,String weight,String fat,String fat_price,String weight_type,String snf,String clr,String total_price,String per_unit_price,String price_calculated_by) {
        loadingView.showDialog();

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> updateCustomerModelCall = apiInterface.updateMilk(mySharedPreference.getUserId(),getUserWiseMilkEntryModel.getMeCustomerId(),customer_code,customer_name,milk_type,selected_date,shift,weight,fat,fat_price,weight_type,snf,clr,total_price,per_unit_price,price_calculated_by,getUserWiseMilkEntryModel.getMeId());
        Log.d(TAG,"id" +mySharedPreference.getUserId());
        updateCustomerModelCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel updateCustomerModel = response.body();
                if(updateCustomerModel != null){
                    if(updateCustomerModel.getResult()){
                        Toast.makeText(getApplicationContext(),updateCustomerModel.getMsg(),Toast.LENGTH_LONG).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),updateCustomerModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),updateCustomerModel.getMsg(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getFixPrice(String fat,String weight,String milkType) {
        GetFixPrice getFixPrice = new GetFixPrice(fat,weight,milkType);
        getFixPrice.execute();
    }

    class GetFixPrice extends AsyncTask<Void,Void,List<FixPriceTable>>{
        String sFat,sWeight,smilkType;

        public GetFixPrice(String sFat, String sWeight, String sm) {
            this.sFat = sFat;
            this.sWeight = sWeight;
            this.smilkType = rbBuffalo.isChecked() ? "buffalo" : "cow";
        }

        @Override
        protected List<FixPriceTable> doInBackground(Void... voids) {
            List<FixPriceTable> fixPriceTableData = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getFixPriceDao().getFixPrice(smilkType);
            Log.d(TAG,"fix price data" +fixPriceTableData);
            return fixPriceTableData;
        }

        @Override
        protected void onPostExecute(List<FixPriceTable> aVoid) {
            super.onPostExecute(aVoid);
            fixPriceTableList.addAll(aVoid);
            for (FixPriceTable fixPriceTable: fixPriceTableList){
                String fatPrice = fixPriceTable.getFwpFatWisePrice();
                Double total = Double.parseDouble(sFat) * Double.parseDouble(sWeight) * Double.parseDouble(fatPrice);
                etAddMilkTotal.setText(String.valueOf(format.format(total)));
            }
        }
    }

    private void getUserWisePriceData(String fat,String weight,String milkType) {
        GetUserPrice getUserPrice = new GetUserPrice(fat,weight,milkType);
        getUserPrice.execute();
    }

    class GetUserPrice extends AsyncTask<Void,Void,List<FatandSNFPriceTable>>{
        String sFat,sWeight,smilkType;
        String fp_rate;
        public GetUserPrice(String sFat, String sWeight, String sm) {
            this.sFat = sFat;
            this.sWeight = sWeight;
            this.smilkType = rbBuffalo.isChecked()?"buffalo":"cow";
        }

        @Override
        protected List<FatandSNFPriceTable> doInBackground(Void... voids) {


            List<FatandSNFPriceTable> fatandSNFPriceTableData = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getFatandSNFPriceDao().getFatandSnfPRiceData(sFat,smilkType,"0","fat");
            if(fatandSNFPriceTableData.size()>0) {
                for (FatandSNFPriceTable fatandSNFPriceTable : fatandSNFPriceTableData) {
                    fs_id = fatandSNFPriceTable.getFsId();
                }
                List<GetUserWisePriceTable> getUserWisePriceTableData = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getUserWisePriceDao().getUserWisePRiceData(fs_id);
                if (getUserWisePriceTableData.size()>0){
                    for (GetUserWisePriceTable getUserWisePriceTable : getUserWisePriceTableData){
                        fp_rate =getUserWisePriceTable.getFpRate();

                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<FatandSNFPriceTable> aVoid) {
            super.onPostExecute(aVoid);
            if(fp_rate != null) {
                Double total = Double.parseDouble(sFat) * Double.parseDouble(sWeight) * Double.parseDouble(fp_rate);
                etAddMilkTotal.setText(String.valueOf(format.format(total)));
            }else {
                etAddMilkTotal.setText(String.valueOf(0));
            }
        }
    }

    private void getFatandSNFWisePriceData(String snf,String fat,String weight,String milkType) {
        GetFatandSNFPrice getFatandSNFPrice = new GetFatandSNFPrice(snf,fat,weight,milkType);
        getFatandSNFPrice.execute();
    }

    class GetFatandSNFPrice extends AsyncTask<Void,Void,List<FatandSNFPriceTable>>{
        String ssnf,sFat,sWeight,smilkType;
        String fp_rate,fs_parent_id;
        public GetFatandSNFPrice(String ssnf,String sFat, String sWeight, String sm) {
            this.ssnf = ssnf;
            this.sFat = sFat;
            this.sWeight = sWeight;
            this.smilkType = rbBuffalo.isChecked()?"buffalo":"cow";
        }

        @Override
        protected List<FatandSNFPriceTable> doInBackground(Void... voids) {
            List<FatandSNFPriceTable> fatandSnfPRiceData = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getFatandSNFPriceDao().getFatandSnfPRiceData(ssnf,smilkType,"0","snf");
            if (fatandSnfPRiceData.size()>0) {
                for (FatandSNFPriceTable fatandSNFPriceTable : fatandSnfPRiceData){
                    fs_parent_id = fatandSNFPriceTable.getFsId();
                }

                List<FatandSNFPriceTable> fatandSNFPriceTableData = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getFatandSNFPriceDao().getFatandSnfPRiceData(sFat, smilkType, fs_parent_id, "fat");
                if (fatandSNFPriceTableData.size() > 0) {
                    for (FatandSNFPriceTable fatandSNFPriceTable : fatandSNFPriceTableData) {
                        fs_id = fatandSNFPriceTable.getFsId();
                    }
                    List<GetUserWisePriceTable> getUserWisePriceTableData = MyRoomDatabase.getRoomDatabase(getApplicationContext()).getUserWisePriceDao().getUserWisePRiceData(fs_id);
                    if (getUserWisePriceTableData.size() > 0) {
                        for (GetUserWisePriceTable getUserWisePriceTable : getUserWisePriceTableData) {
                            fp_rate = getUserWisePriceTable.getFpRate();

                        }
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<FatandSNFPriceTable> aVoid) {
            super.onPostExecute(aVoid);
            if(fp_rate != null) {
                Double total = Double.parseDouble(sFat) * Double.parseDouble(sWeight) * Double.parseDouble(fp_rate);
                etAddMilkTotal.setText(String.valueOf(format.format(total)));
            }else {
                etAddMilkTotal.setText(String.valueOf(0));
            }
        }
    }
}
