package com.example.mymilkcollectionapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.FatWisePriceModel;
import com.example.mymilkcollectionapp.model.FatWisePriceResponseModel;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FatandSnfWisePriceFragment extends Fragment {

    @BindView(R.id.llContainer)
    LinearLayout llContainer;
    String milkType;
    LoadingView loadingView;
    MySharedPreference mySharedPreference;
    View view;
    List<View> viewList;
    FatWisePriceModel fatWisePriceModel;

    List<FatWisePriceModel> fatWisePriceModelList;
    public FatandSnfWisePriceFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_fatand_snf_wise_price, container, false);
        ButterKnife.bind(this,rootView);
        initAllControls();
        return rootView;
    }

    private void initAllControls() {
        milkType = this.getArguments().getString("milk_data");
        fatWisePriceModel = this.getArguments().getParcelable("data");
        loadingView = new LoadingView(getContext());
        mySharedPreference = new MySharedPreference(getContext());
        viewList = new ArrayList<>();
        fatWisePriceModelList = new ArrayList<>();
        getFatWiseDatafromServer();
    }
    @OnClick(R.id.btnFatWisePriceUpdate)
    public void onbtnFatWisePriceUpdate(){
        if(viewList.isEmpty()){
            Toast.makeText(getContext(),"ViewList empty",Toast.LENGTH_LONG).show();
            return;
        }else {
            int position = 0;
            for (View view:viewList){
                TextView tvFat = view.findViewById(R.id.tvFat);
                EditText etRate = view.findViewById(R.id.etRate);
                String sRate = etRate.getText().toString();
                if (sRate.trim().length() == 0){
                    Toast.makeText(getContext(),"rate empty",Toast.LENGTH_LONG).show();
                    return;
                }else {
                    FatWisePriceModel fatWisePriceModel = fatWisePriceModelList.get(position);
                    fatWisePriceModel.setFpRate(sRate);
                }position++;
            }

            Gson gson = new GsonBuilder().create();
            JsonArray jsonArray = gson.toJsonTree(fatWisePriceModelList).getAsJsonArray();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("result",true);
            jsonObject.add("data",jsonArray);
            sendUpdateRate(jsonObject);
        }
    }
    public void getFatWiseDatafromServer(){

        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FatWisePriceResponseModel> fatWisePriceResponseModelCall = apiInterface.getFatWisePrice(mySharedPreference.getUserId(),"fat",milkType,fatWisePriceModel.getFsId());
        fatWisePriceResponseModelCall.enqueue(new Callback<FatWisePriceResponseModel>() {
            @Override
            public void onResponse(Call<FatWisePriceResponseModel> call, Response<FatWisePriceResponseModel> response) {
                loadingView.hideDialog();
                FatWisePriceResponseModel fatWisePriceResponseModel = response.body();
                if(fatWisePriceResponseModel != null){
                    if(fatWisePriceResponseModel.getResult()){
                        for (FatWisePriceModel fatWisePriceModel:fatWisePriceResponseModel.getData()){
                            view = getLayoutInflater().inflate(R.layout.fatwise_price_inflater,null);
                            TextView tvFat = view.findViewById(R.id.tvFat);
                            EditText etRate = view.findViewById(R.id.etRate);
                            tvFat.setText(fatWisePriceModel.getFsValue());
                            etRate.setText(fatWisePriceModel.getFpRate());
                            Log.d("FatWisePriceActivity","rate "+etRate);
                            llContainer.addView(view);
                            viewList.add(view);
                        }
                        fatWisePriceModelList.addAll(fatWisePriceResponseModel.getData());
                    }
                    else {
                        Toast.makeText(getContext(),"No data found",Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FatWisePriceResponseModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }

    public void sendUpdateRate(JsonObject object){
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> statusModelCall = apiInterface.updateAdminFatRate(object);
        statusModelCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel statusModel = response.body();
                if(statusModel != null){
                    if(statusModel.getResult()){
                        Toast.makeText(getContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
