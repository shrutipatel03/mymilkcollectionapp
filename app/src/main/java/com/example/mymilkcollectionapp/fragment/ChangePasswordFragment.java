package com.example.mymilkcollectionapp.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.activity.RegisterActivity;
import com.example.mymilkcollectionapp.activity.loginActivity;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.model.UserRegisterModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.LoadingView;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePasswordFragment extends Fragment {

    @BindView(R.id.etCurrentPassword)
    EditText etCurrentPassword;
    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;
    LoadingView loadingView;
    MySharedPreference mySharedPreference;

    public ChangePasswordFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this,rootView);
        initAllControls();
        return rootView;
    }

    private void initAllControls() {
        loadingView = new LoadingView(getContext());
        mySharedPreference = new MySharedPreference(getContext());
    }

    @OnClick(R.id.btnChangePassword)
    public void onbtnChangePassword(){
        String sCurrentPassword = etCurrentPassword.getText().toString();
        String sNewPassword = etNewPassword.getText().toString();
        String sConfirmPassword = etConfirmPassword.getText().toString();

        if (sCurrentPassword.isEmpty()){
            Snackbar.make(etCurrentPassword,"enter current password",Snackbar.LENGTH_LONG).show();
            return;
        }else if (sCurrentPassword.length()<6){
            Snackbar.make(etCurrentPassword,"current password should be 6 character long",Snackbar.LENGTH_LONG).show();
            return;
        }
        else if (sNewPassword.isEmpty()){
            Snackbar.make(etNewPassword,"enter new password",Snackbar.LENGTH_LONG).show();
            return;
        }else if (sNewPassword.length()<6){
            Snackbar.make(etNewPassword,"new password should be 6 charcter long",Snackbar.LENGTH_LONG).show();
            return;
        }
        else if (sConfirmPassword.isEmpty()){
            Snackbar.make(etConfirmPassword,"enter confirm password",Snackbar.LENGTH_LONG).show();
            return;
        }else if (!sNewPassword.equals(sConfirmPassword)){
            Snackbar.make(etConfirmPassword,"confirm password not match",Snackbar.LENGTH_LONG).show();
            return;
        }

        sendChangePasswordToServer(sNewPassword);
    }

    private void sendChangePasswordToServer(String userPassword) {
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> changeUserPasswordCall  = apiInterface.changeUserPassword(userPassword,mySharedPreference.getUserId());


        changeUserPasswordCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel statusModel = response.body();
                if(statusModel != null){
                    if(statusModel.getResult()){
                        Toast.makeText(getContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();

                    }
                    else {
                        Toast.makeText(getContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }
}
