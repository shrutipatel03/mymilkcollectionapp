package com.example.mymilkcollectionapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.activity.AddMilkActivity;
import com.example.mymilkcollectionapp.activity.CustomersActivity;
import com.example.mymilkcollectionapp.activity.ReportsActivity;
import com.example.mymilkcollectionapp.activity.SettingsActivity;
import com.example.mymilkcollectionapp.activity.ShiftWiseGeneralReportActivity;
import com.example.mymilkcollectionapp.activity.UpdateRateActivity;
import com.example.mymilkcollectionapp.activity.ViewRecordActivity;


import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,root);
        initAllControls();
        return root;
    }

    private void initAllControls() {
    }

    @OnClick(R.id.cvCustomer)
    public void oncvAddCustomer(View view){
        Intent intent = new Intent(getContext(), CustomersActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvAddMilk)
    public void  oncvAddMilk(){
        Intent intent = new Intent(getContext(), AddMilkActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.cvViewRecord)
    public void  oncvViewRecord(){
        Intent intent = new Intent(getContext(), ViewRecordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvUpdateRate)
    public void  oncvUpdateRate(){
        Intent intent = new Intent(getContext(), UpdateRateActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvSettings)
    public void  oncvSettings(){
        Intent intent = new Intent(getContext(), SettingsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvReports)
    public void  oncvReports(){
        Intent intent = new Intent(getContext(), ReportsActivity.class);
        startActivity(intent);
    }
}
