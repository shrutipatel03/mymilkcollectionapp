package com.example.mymilkcollectionapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.activity.MainActivity;
import com.example.mymilkcollectionapp.database.MySharedPreference;
import com.example.mymilkcollectionapp.model.StatusModel;
import com.example.mymilkcollectionapp.myUtils.ApiClient;
import com.example.mymilkcollectionapp.myUtils.ApiInterface;
import com.example.mymilkcollectionapp.myUtils.EditUserProfileDialogInterface;
import com.example.mymilkcollectionapp.myUtils.GlobalMethods;
import com.example.mymilkcollectionapp.myUtils.LoadingView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UserProfileFragment extends Fragment {

    @BindView(R.id.tvDairyName)
    TextView tvDairyName;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvUserMobile)
    TextView tvUserMobile;
    @BindView(R.id.tvUserEmail)
    TextView tvUserEmail;
    MySharedPreference mySharedPreference;
    LoadingView loadingView;
    public UserProfileFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_user_profile, container, false);
        ButterKnife.bind(this,rootView);
        initAllControls();
        return rootView;
    }

    private void initAllControls() {
        loadingView = new LoadingView(getContext());
        mySharedPreference = new MySharedPreference(getContext());
        tvDairyName.setText(mySharedPreference.getUserDairyName());
        tvUserName.setText(mySharedPreference.getUserName());
        tvUserEmail.setText(mySharedPreference.getEmail());
        tvUserMobile.setText(mySharedPreference.getMobileNumber());
    }

    @OnClick(R.id.ivUpdateDairyName)
    public void onUpdateDairyName(View view){

        GlobalMethods.editUSerProfileDialog(getContext(), new EditUserProfileDialogInterface(){
            @Override
            public void onUpdateInterface(String enterValue) {
                tvDairyName.getText().toString();
                String changeType = "user_dairy_name";
                sendEditProfileToServer(changeType,enterValue);
            }

            @Override
            public void onCancleInterface() {

            }
        },"Change Dairy name",tvDairyName.getText().toString());
    }
    @OnClick(R.id.ivUpdateUserName)
    public void onUpdateUserName(View view){
        GlobalMethods.editUSerProfileDialog(getContext(), new EditUserProfileDialogInterface() {
            @Override
            public void onUpdateInterface(String enterValue) {
                tvUserName.getText().toString();
                String changeType = "user_name";
                sendEditProfileToServer(changeType,enterValue);
            }

            @Override
            public void onCancleInterface() {

            }
        },"Change User name",tvUserName.getText().toString());
    }
    @OnClick(R.id.ivUpdateUserEmail)
    public void onUpdateUserEmail(View view){
        GlobalMethods.editUSerProfileDialog(getContext(), new EditUserProfileDialogInterface() {
            @Override
            public void onUpdateInterface(String enterValue) {
                tvUserEmail.getText().toString();
                String changeType = "user_email";
                sendEditProfileToServer(changeType,enterValue);
            }

            @Override
            public void onCancleInterface() {

            }
        },"Change user email",tvUserEmail.getText().toString());
    }
    private void sendEditProfileToServer(String change_type,String value) {
        loadingView.showDialog();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusModel> editProfileCall  = apiInterface.updateUserDetail(mySharedPreference.getUserId(),change_type,value);


        editProfileCall.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                loadingView.hideDialog();
                StatusModel statusModel = response.body();
                if(statusModel != null){
                    if(statusModel.getResult()){
                        Toast.makeText(getContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();
                        if (change_type.equals("user_dairy_name")){
                            mySharedPreference.setDairyName(value);
                            tvDairyName.setText(value);
                        }else if (change_type.equals("user_name")){
                            mySharedPreference.setUserName(value);
                            tvUserName.setText(value);
                            if (MainActivity.getMainActivity()!=null){
                                MainActivity.getMainActivity().setNavigationHeader();
                            }
                        }else if (change_type.equals("user_email")){
                            mySharedPreference.setEmail(value);
                            tvUserName.setText(value);
                            if (MainActivity.getMainActivity()!=null){
                                MainActivity.getMainActivity().setNavigationHeader();
                            }
                        }
                    }
                    else {
                        Toast.makeText(getContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getContext(),statusModel.getMsg(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                loadingView.hideDialog();
            }
        });
    }

}
