package com.example.mymilkcollectionapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.model.CustomerWiseReportModel;

import java.util.List;

public class CustomerWiseReportAdapter extends RecyclerView.Adapter<CustomerWiseReportAdapter.CustomerReportViewHolder> {
    Context context;
    List<CustomerWiseReportModel> customerWiseReportModelList;




    public CustomerWiseReportAdapter(Context context, List<CustomerWiseReportModel> customerWiseReportModelList) {
        this.context = context;
        this.customerWiseReportModelList = customerWiseReportModelList;

    }

    @NonNull
    @Override
    public CustomerWiseReportAdapter.CustomerReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_wise_report_inflater,parent,false);
        return new CustomerWiseReportAdapter.CustomerReportViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull CustomerWiseReportAdapter.CustomerReportViewHolder holder, int position) {
        CustomerWiseReportModel customerWiseReportModel = customerWiseReportModelList.get(position);
        holder.tvDate.setText(customerWiseReportModel.getMeDate());
        holder.tvMilkType.setText(customerWiseReportModel.getMeMilkType());
        holder.tvWeigthType.setText(customerWiseReportModel.getMeWeightType());
        holder.tvFatRate.setText(customerWiseReportModel.getMeFatPrice());
        holder.tvShift.setText(customerWiseReportModel.getMeShift());
        holder.tvRatePerLtr.setText(customerWiseReportModel.getMePerUnitPrice());
        holder.tvFat.setText(customerWiseReportModel.getMeFat());
        holder.tvTotalRs.setText(customerWiseReportModel.getMeTotalPrice());
    }

    @Override
    public int getItemCount() {
        return customerWiseReportModelList.size();
    }


    public class CustomerReportViewHolder extends RecyclerView.ViewHolder{
        TextView tvDate,tvMilkType,tvWeigthType,tvFat,tvFatRate,tvShift,tvRatePerLtr,tvTotalRs;

        public CustomerReportViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvMilkType = itemView.findViewById(R.id.tvMilkType);
            tvWeigthType = itemView.findViewById(R.id.tvWeigthType);
            tvFatRate = itemView.findViewById(R.id.tvFatRate);
            tvShift = itemView.findViewById(R.id.tvMilkShift);
            tvRatePerLtr = itemView.findViewById(R.id.tvRatePerLtr);
            tvFat = itemView.findViewById(R.id.tvFat);
            tvTotalRs = itemView.findViewById(R.id.tvTotalRs);

        }
    }
}
