package com.example.mymilkcollectionapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.model.GetCustomerListModel;
import com.example.mymilkcollectionapp.model.GetCustomerListResponseModel;

import java.util.ArrayList;
import java.util.List;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerListViewHolder> implements Filterable {

    Context context;
    List<GetCustomerListModel> getCustomerListModelList;
    List<GetCustomerListModel> filterableDataList;
    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemUpdateListener(int position);
        void onItemDeleteListener(int position);
    }

    public CustomerListAdapter(Context context, List<GetCustomerListModel> getCustomerListModelList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.getCustomerListModelList = getCustomerListModelList;
        filterableDataList = getCustomerListModelList;
        this.onItemClickListener = onItemClickListener;
    }
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequence = constraint.toString();
                if (charSequence.isEmpty()){
                    filterableDataList = getCustomerListModelList;
                }
                else {
                    List<GetCustomerListModel> customerListModels = new ArrayList<>();
                    for (GetCustomerListModel row:getCustomerListModelList){
                        if(row.getCustomerName().toLowerCase().contains(charSequence.toLowerCase())) {
                            customerListModels.add(row);

                        }
                    }filterableDataList = customerListModels;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterableDataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filterableDataList =(ArrayList<GetCustomerListModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }




    @NonNull
    @Override
    public CustomerListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_list_inflater,parent,false);
        return new CustomerListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerListViewHolder holder, int position) {
        GetCustomerListModel getCustomerListModel = filterableDataList.get(position);
        holder.ivUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemUpdateListener(position);
            }
        });
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemDeleteListener(position);
            }
        });
        holder.tvCustomerCode.setText(getCustomerListModel.getCustomerCode());
        holder.tvCustomerName.setText(getCustomerListModel.getCustomerName());
        holder.tvCustomerMobile.setText(getCustomerListModel.getCustomerMobileNumber());
    }

    @Override
    public int getItemCount() {
        return filterableDataList.size();
    }

    public class CustomerListViewHolder extends RecyclerView.ViewHolder{
        public TextView tvCustomerCode,tvCustomerName,tvCustomerMobile;
        public ImageView ivUpdate,ivDelete;
        public CustomerListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCustomerCode = itemView.findViewById(R.id.tvCustomerCode);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvCustomerMobile = itemView.findViewById(R.id.tvCustomerMobile);
            ivUpdate = itemView.findViewById(R.id.ivUpdate);
            ivDelete = itemView.findViewById(R.id.ivDelete);
        }
    }
}
