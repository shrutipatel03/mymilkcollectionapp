package com.example.mymilkcollectionapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.model.PaymentReportModel;

import java.util.List;

public class PaymentReportAdapter extends RecyclerView.Adapter<PaymentReportAdapter.PaymentViewHolder> {
    Context context;
    List<PaymentReportModel> paymentReportAdapterList;



    public PaymentReportAdapter(Context context, List<PaymentReportModel> paymentReportAdapterList) {
        this.context = context;
        this.paymentReportAdapterList = paymentReportAdapterList;

    }

    @NonNull
    @Override
    public PaymentReportAdapter.PaymentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_report_inflater,parent,false);
        return new PaymentReportAdapter.PaymentViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull PaymentReportAdapter.PaymentViewHolder holder, int position) {
        PaymentReportModel paymentReportModel = paymentReportAdapterList.get(position);
        holder.tvCustomerCode.setText(paymentReportModel.getMeCustomerCode());
        holder.tvCustomerName.setText(paymentReportModel.getMeCustomerName());
        holder.tvTotalRs.setText(paymentReportModel.getTotalAmount());
        holder.tvWeigthType.setText(paymentReportModel.getMeWeightType());
    }

    @Override
    public int getItemCount() {
        return paymentReportAdapterList.size();
    }


    public class PaymentViewHolder extends RecyclerView.ViewHolder{
        TextView tvCustomerName,tvCustomerCode,tvWeigthType,tvTotalRs;

        public PaymentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCustomerCode = itemView.findViewById(R.id.tvCustomerCode);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvWeigthType = itemView.findViewById(R.id.tvWeigthType);
            tvTotalRs = itemView.findViewById(R.id.tvTotalRs);

        }
    }
}
