package com.example.mymilkcollectionapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymilkcollectionapp.R;
import com.example.mymilkcollectionapp.model.GetCustomerListModel;
import com.example.mymilkcollectionapp.model.GetUserWiseMilkEntryModel;

import java.util.List;

public class ViewRecordAdapter extends RecyclerView.Adapter<ViewRecordAdapter.RecordViewHolder> {
    Context context;
    List<GetUserWiseMilkEntryModel> getUserWiseMilkEntryModelList;
    ViewRecordAdapter.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemMilkDataUpdateListener(int position);
        void onItemMilkDataDeleteListener(int position);
    }

    public ViewRecordAdapter(Context context, List<GetUserWiseMilkEntryModel> getUserWiseMilkEntryModelList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.getUserWiseMilkEntryModelList = getUserWiseMilkEntryModelList;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewRecordAdapter.RecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.milk_list_inflater,parent,false);
        return new RecordViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull ViewRecordAdapter.RecordViewHolder holder, int position) {
        GetUserWiseMilkEntryModel getUserWiseMilkEntryModel = getUserWiseMilkEntryModelList.get(position);
        holder.btnUpadte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemMilkDataUpdateListener(position);
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemMilkDataDeleteListener(position);
            }
        });
        holder.tvCustomerCode.setText(getUserWiseMilkEntryModel.getMeCustomerCode());
        holder.tvCustomerName.setText(getUserWiseMilkEntryModel.getMeCustomerName());
        holder.tvMilkType.setText(getUserWiseMilkEntryModel.getMeMilkType());
        holder.tvWeigthType.setText(getUserWiseMilkEntryModel.getMeWeightType());
        holder.tvFat.setText(getUserWiseMilkEntryModel.getMeFat());
        holder.tvFatPrice.setText(getUserWiseMilkEntryModel.getMeFatPrice());
        holder.tvSNF.setText(getUserWiseMilkEntryModel.getMeSnf());
        holder.tvTotalRate.setText(getUserWiseMilkEntryModel.getMeTotalPrice());
    }

    @Override
    public int getItemCount() {
        return getUserWiseMilkEntryModelList.size();
    }


    public class RecordViewHolder extends RecyclerView.ViewHolder{
        TextView tvCustomerCode,tvCustomerName,tvMilkType,tvWeigthType,tvFat,tvFatPrice,tvSNF,tvTotalRate;
        Button btnUpadte,btnDelete;
        public RecordViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCustomerCode = itemView.findViewById(R.id.tvCustomerCode);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvMilkType = itemView.findViewById(R.id.tvMilkType);
            tvWeigthType = itemView.findViewById(R.id.tvWeigthType);
            tvFat = itemView.findViewById(R.id.tvFat);
            tvFatPrice = itemView.findViewById(R.id.tvFatPrice);
            tvSNF = itemView.findViewById(R.id.tvSNF);
            tvTotalRate = itemView.findViewById(R.id.tvTotalRate);
            btnUpadte = itemView.findViewById(R.id.btnUpadte);
            btnDelete = itemView.findViewById(R.id.btnDelete);
        }
    }
}
