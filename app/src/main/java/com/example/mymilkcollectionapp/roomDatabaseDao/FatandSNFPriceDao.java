package com.example.mymilkcollectionapp.roomDatabaseDao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.mymilkcollectionapp.roomDatabaseTable.FatandSNFPriceTable;
import com.example.mymilkcollectionapp.roomDatabaseTable.FixPriceTable;

import java.util.List;

@Dao
public interface FatandSNFPriceDao {
    @Insert
    long[] insertMultipleData(List<FatandSNFPriceTable> fatandSNFPriceTableList);

    @Query("Delete from FatandSNFPriceTable")
    int deleteAll();

    @Query("select * from FatandSNFPriceTable where fs_value=:value and fs_milk_type=:milkType and fs_parent_id=:parentId and fs_type=:type")
    List<FatandSNFPriceTable> getFatandSnfPRiceData(String value,String milkType,String parentId,String type);
}
