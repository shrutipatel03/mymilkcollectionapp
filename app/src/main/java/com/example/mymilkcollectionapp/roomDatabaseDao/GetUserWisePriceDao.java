package com.example.mymilkcollectionapp.roomDatabaseDao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.mymilkcollectionapp.roomDatabaseTable.FatandSNFPriceTable;
import com.example.mymilkcollectionapp.roomDatabaseTable.GetUserWisePriceTable;

import java.util.List;

@Dao
public interface GetUserWisePriceDao {
    @Insert
    long[] insertMultipleData(List<GetUserWisePriceTable> getUserWisePriceTableList);

    @Query("Delete from GetUserWisePriceTable")
    int deleteAll();

    @Query("select * from GetUserWisePriceTable where fp_fs_id=:rate")
    List<GetUserWisePriceTable> getUserWisePRiceData(String rate);
}
