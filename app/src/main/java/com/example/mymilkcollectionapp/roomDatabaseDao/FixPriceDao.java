package com.example.mymilkcollectionapp.roomDatabaseDao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.mymilkcollectionapp.roomDatabaseTable.FixPriceTable;

import java.util.List;

@Dao
public interface FixPriceDao {

    @Insert
    long[] insertMultipleData(List<FixPriceTable> fixPriceTableList);

    @Query("Delete from FixPriceTable")
    int DeleteAll();

    @Query("select * from FixPriceTable where fwp_milk_type=:milk_type")
    List<FixPriceTable> getFixPrice(String milk_type);
}
